#include "Smoothfit.h"
#include <stack>
#include <cmath>
#include <algorithm>
#include <float.h>
#include <eigen3/Eigen/Dense>
#include "interpolateDisparityImage.h"

struct DisparityPixelS {
	double normalizedX;
	double normalizedY;
	double disparity;
};


// Default parameter
const int SMOOTHFIT_DEFAULT_ITERATION_TOTAL = 10;
const double SMOOTHFIT_DEFAULT_COMPACTNESS_WEIGHT = 500.0;
const double SMOOTHFIT_DEFAULT_DISPARITY_WEIGHT = 2000.0;
const double SMOOTHFIT_DEFAULT_NO_DISPARITY_PENALTY = 3.0;

// Pixel offsets of 4- and 8-neighbors
const int fourNeighborTotal = 4;
const int fourNeighborOffsetX[4] = {-1, 0, 1, 0};
const int fourNeighborOffsetY[4] = { 0,-1, 0, 1};
const int eightNeighborTotal = 8;
const int eightNeighborOffsetX[8] = {-1,-1, 0, 1, 1, 1, 0,-1};
const int eightNeighborOffsetY[8] = { 0,-1,-1,-1, 0, 1, 1, 1};

// Prototype declaration
int computeRequiredSamplingTotal(const int drawTotal, const int inlierTotal, const int pointTotal,
								 const int currentSamplingTotal, const double confidenceLevel);


Smoothfit::Smoothfit() : iterationTotal_(SMOOTHFIT_DEFAULT_ITERATION_TOTAL),
	compactnessWeight_(SMOOTHFIT_DEFAULT_COMPACTNESS_WEIGHT),
	disparityWeight_(SMOOTHFIT_DEFAULT_DISPARITY_WEIGHT),
	noDisparityPenalty_(SMOOTHFIT_DEFAULT_NO_DISPARITY_PENALTY) {}

void Smoothfit::setIterationTotal(const int iterationTotal) {
	if (iterationTotal < 1) {
		throw rev::Exception("Smoothfit::setIterationTotal", "the number of iterations is less than 1");
	}

	iterationTotal_ = iterationTotal;
}

void Smoothfit::setWeightParameters(const double compactnessWeight,
									 const double disparityWeight,
									 const double noDisparityPenalty)
{
	if (compactnessWeight <= 0) {
		throw rev::Exception("StereoSlic::setWeightParameters", "compactness weight is less than zero");
	}
	compactnessWeight_ = compactnessWeight;

	if (disparityWeight < 0) {
		throw rev::Exception("StereoSlic::setWeightParameters", "weight of disparity term is less than zero");
	}
	disparityWeight_ = disparityWeight;

	if (noDisparityPenalty < 0) {
		throw rev::Exception("StereoSlic::setWeightParameters", "penalty value of no disparity is less than zero");
	}
	noDisparityPenalty_ = noDisparityPenalty;
}

void Smoothfit::segment(const int superpixelTotal,
							const rev::Image<unsigned char>& firstLeftImage,
							const rev::Image<unsigned short>& firstLeftDisparityShortImage,
							const double disparityFactor,
							rev::Image<unsigned short>& segmentImage,
							rev::Image<unsigned short>& segmentDisparityImage,
							rev::Image<unsigned char>& boundaryLabelImage,
							std::vector< std::vector<int> >& boundaryLabeList,
							std::vector< std::vector<double> >& disparityPlanes)
{
	if (superpixelTotal < 2) {
		throw rev::Exception("Smoothfit::segment", "the number of superpixel is less than 2");
	}

	setColorAndDisparity(firstLeftImage, firstLeftDisparityShortImage, disparityFactor);
	//CalibratedEpipolarFlowGeometry epipolarFlowGeometry(firstLeftImage.width(), firstLeftImage.height(), cameraMotion);

	initializeSeeds(superpixelTotal);
	//performSegmentation(cameraMotion, epipolarFlowGeometry);
	performSegmentation();

	int width = firstLeftLabImage_.width();
	int height = firstLeftLabImage_.height();
	segmentImage.resize(width, height, 1);
	for (int y = 0; y < height; ++y) {
		for (int x = 0; x < width; ++x) {
			segmentImage(x, y) = labels_[width*y + x];
		}
	}

	makeDisparityImage(disparityFactor, segmentDisparityImage);
    //by Harry
    saveDisparityPlanes(disparityFactor, disparityPlanes);

	//makeFlowImage(segmentDisparityImage, cameraMotion, epipolarFlowGeometry, segmentFlowImage);

	boundaryLabelImage = boundaryLabelImage_;
	boundaryLabeList = boundaryLabelList_;
}


void Smoothfit::setColorAndDisparity(const rev::Image<unsigned char>& firstLeftImage,
									 const rev::Image<unsigned short>& firstLeftDisparityShortImage,
									 const double disparityFactor)
{
	checkInputImages(firstLeftImage, firstLeftDisparityShortImage);

	setStereoLabImages(firstLeftImage);
	calcLeftLabEdges();
	setDisparityImages(firstLeftDisparityShortImage, disparityFactor);
}

void Smoothfit::initializeSeeds(const int superpixelTotal) {
	setGridSeedPoint(superpixelTotal);
	perturbSeeds();
	initializeAssignment();

	outlierImage_.resize(firstLeftLabImage_.width(), firstLeftLabImage_.height(), 1);
	outlierImage_.setTo(0);
}

/*
void Smoothfit::performSegmentation(const CameraMotion& cameraMotion, const CalibratedEpipolarFlowGeometry& epipolarFlowGeometry) {
	initialFlag_ = true;
	for (int iterationCount = 0; iterationCount < iterationTotal_; ++iterationCount) {
		assignLabelWithConnectivity(cameraMotion, epipolarFlowGeometry);
		updateSeeds(cameraMotion, epipolarFlowGeometry);
	}
}
*/

void Smoothfit::performSegmentation() {
	initialFlag_ = true;
	for (int iterationCount = 0; iterationCount < iterationTotal_; ++iterationCount) {
		assignLabelWithConnectivity();
		updateSeeds();
	}
}

void Smoothfit::makeDisparityImage(const double disparityFactor, rev::Image<unsigned short>& disparityImage) {
	double maxDisparity = 65536/disparityFactor;

	int width = firstLeftLabImage_.width();
	int height = firstLeftLabImage_.height();
	disparityImage.resize(width, height, 1);
	for (int y = 0; y < height; ++y) {
		for (int x = 0; x < width; ++x) {
			int seedIndex = labels_[width*y + x];
			double estimatedDisparity = seeds_[seedIndex].disparityPlane[0]*x
				+ seeds_[seedIndex].disparityPlane[1]*y
				+ seeds_[seedIndex].disparityPlane[2];
			if (estimatedDisparity > 0.0 && estimatedDisparity < maxDisparity) {
				disparityImage(x, y) = static_cast<unsigned short>(estimatedDisparity*disparityFactor + 0.5);
			} else if (estimatedDisparity >= maxDisparity) {
				disparityImage(x, y) = 65535;
			} else {
				disparityImage(x, y) = 0;
			}
			if (disparityImage(x, y) == 0) disparityImage(x, y) = 1;
		}
	}
}

void Smoothfit::saveDisparityPlanes(const double disparityFactor, std::vector<std::vector< double> >& disparityPlanes)
{
    double maxDisparity = 65536/disparityFactor;
    int planeNum=seeds_.size();
    std::vector<bool> vFlag(planeNum);
    fill(vFlag.begin(),vFlag.end(),true);
    int width = firstLeftLabImage_.width();
	int height = firstLeftLabImage_.height();
    for(int y=0;y<height;y++)
    {
        for(int x=0;x<width;x++)
        {
        int seedIndex = labels_[width*y + x];
        double estimatedDisparity = seeds_[seedIndex].disparityPlane[0]*x
				+ seeds_[seedIndex].disparityPlane[1]*y
				+ seeds_[seedIndex].disparityPlane[2];
            if(estimatedDisparity>=maxDisparity || estimatedDisparity<=0.0)
            {
                vFlag[seedIndex]=false;
            }
        }
    }
    disparityPlanes.resize(planeNum);
    for(int i=0;i<planeNum;i++)
    {
        disparityPlanes[i].resize(4);
        for(int j=0;j<3;j++)
            disparityPlanes[i][j]=seeds_[i].disparityPlane[j];
        if(vFlag[i]==true)
            disparityPlanes[i][3]=1;
        else
            disparityPlanes[i][3]=0;
    }
}

void Smoothfit::checkInputImages(const rev::Image<unsigned char>& firstLeftImage,
									 const rev::Image<unsigned short>& firstLeftDisparityShortImage) const
{
	int width = firstLeftImage.width();
	int height = firstLeftImage.height();
	if (firstLeftDisparityShortImage.width() != width || firstLeftDisparityShortImage.height() != height) {
		throw rev::Exception("Smoothfit::checkInputImages", "sizes of input images are not the same");
	}

	if (firstLeftDisparityShortImage.channelTotal() != 1) {
		throw rev::Exception("Smoothfit::checkInputImages", "disparity image is not a single channel image");
	}
}

void Smoothfit::setStereoLabImages(const rev::Image<unsigned char>& firstLeftImage) {
	rev::Image<unsigned char> firstLeftRgbColorImage = rev::convertToColor(firstLeftImage);
	rev::convertImageRGBToLab(firstLeftRgbColorImage, firstLeftLabImage_);
}

void Smoothfit::calcLeftLabEdges() {
	int width = firstLeftLabImage_.width();
	int height = firstLeftLabImage_.height();
	firstLeftLabEdges_.resize(width*height, 0);
	for (int y = 1; y < height-1; ++y) {
		for (int x = 1; x < width-1; ++x) {
			double dxL = firstLeftLabImage_(x-1, y, 0) - firstLeftLabImage_(x+1, y, 0);
			double dxA = firstLeftLabImage_(x-1, y, 1) - firstLeftLabImage_(x+1, y, 1);
			double dxB = firstLeftLabImage_(x-1, y, 2) - firstLeftLabImage_(x+1, y, 2);
			double dx = dxL*dxL + dxA*dxA + dxB*dxB;

			double dyL = firstLeftLabImage_(x, y-1, 0) - firstLeftLabImage_(x, y+1, 0);
			double dyA = firstLeftLabImage_(x, y-1, 1) - firstLeftLabImage_(x, y+1, 1);
			double dyB = firstLeftLabImage_(x, y-1, 2) - firstLeftLabImage_(x, y+1, 2);
			double dy = dyL*dyL + dyA*dyA + dyB*dyB;

			firstLeftLabEdges_[width*y + x] = dx + dy;
		}
	}
}

void Smoothfit::setDisparityImages(const rev::Image<unsigned short>& firstLeftDisparityShortImage,
									   const double disparityFactor)
{
	int width = firstLeftDisparityShortImage.width();
	int height = firstLeftDisparityShortImage.height();

	firstLeftDisparityImage_.resize(width, height, 1);
	for (int y = 0; y < height; ++y) {
		for (int x = 0; x < width; ++x) {
			firstLeftDisparityImage_(x, y) = static_cast<float>(firstLeftDisparityShortImage(x, y))/disparityFactor;
		}
	}
}

void Smoothfit::setGridSeedPoint(const int superpixelTotal) {
	int width = firstLeftLabImage_.width();
	int height = firstLeftLabImage_.height();
	int imageSize = width*height;

	gridSize_ = sqrt(static_cast<double>(imageSize)/superpixelTotal);
	stepSize_ = static_cast<int>(gridSize_ + 2.0);
	int offsetX = static_cast<int>(gridSize_/2.0);
	int offsetY = static_cast<int>(gridSize_/2.0);

	seeds_.clear();
	for (int indexY = 0; indexY < height; ++indexY) {
		int y = static_cast<int>(gridSize_*indexY + offsetY + 0.5);
		if (y >= height) break;
		for (int indexX = 0; indexX < width; ++indexX) {
			int x = static_cast<int>(gridSize_*indexX + offsetX + 0.5);
			if (x >= width) break;

			LabXYD newSeed;
			newSeed.color[0] = firstLeftLabImage_(x, y, 0);
			newSeed.color[1] = firstLeftLabImage_(x, y, 1);
			newSeed.color[2] = firstLeftLabImage_(x, y, 2);
			newSeed.position[0] = x;
			newSeed.position[1] = y;

			newSeed.pixelTotal = 0;
			newSeed.colorSum[0] = 0;  newSeed.colorSum[1] = 0;  newSeed.colorSum[2] = 0;
			newSeed.positionSum[0] = 0;  newSeed.positionSum[1] = 0;

			seeds_.push_back(newSeed);
		}
	}

	labelTotal_ = static_cast<int>(seeds_.size());
}

void Smoothfit::perturbSeeds() {
	int width = firstLeftLabImage_.width();
	int height = firstLeftLabImage_.height();

	int seedTotal = static_cast<int>(seeds_.size());
	for (int seedIndex = 0; seedIndex < seedTotal; ++seedIndex) {
		int originalX = seeds_[seedIndex].position[0];
		int originalY = seeds_[seedIndex].position[1];
		int originalPixelIndex = width*originalY + originalX;

		int perturbedPixelIndex = originalPixelIndex;
		for (int neighborIndex = 0; neighborIndex < eightNeighborTotal; ++neighborIndex) {
			int neighborX = originalX + eightNeighborOffsetX[neighborIndex];
			int neighborY = originalY + eightNeighborOffsetY[neighborIndex];
			if (neighborX < 0 || neighborX >= width || neighborY < 0 || neighborY >= height) continue;
			int neighborPixelIndex = width*neighborY + neighborX;

			if (firstLeftLabEdges_[neighborPixelIndex] < firstLeftLabEdges_[perturbedPixelIndex]) {
				perturbedPixelIndex = neighborPixelIndex;
			}
		}

		if (perturbedPixelIndex != originalPixelIndex) {
			int perturbedX = perturbedPixelIndex%width;
			int perturbedY = perturbedPixelIndex/width;

			seeds_[seedIndex].color[0] = firstLeftLabImage_(perturbedX, perturbedY, 0);
			seeds_[seedIndex].color[1] = firstLeftLabImage_(perturbedX, perturbedY, 1);
			seeds_[seedIndex].color[2] = firstLeftLabImage_(perturbedX, perturbedY, 2);
			seeds_[seedIndex].position[0] = perturbedX;
			seeds_[seedIndex].position[1] = perturbedY;
		}
	}
}

void Smoothfit::initializeAssignment() {
	int width = firstLeftLabImage_.width();
	int height = firstLeftLabImage_.height();
	int seedTotal = static_cast<int>(seeds_.size());

	labels_.resize(width*height, -1);
	std::vector<double> distancesToSeeds(width*height, DBL_MAX);
	for (int seedIndex = 0; seedIndex < seedTotal; ++seedIndex) {
		int minX = (seeds_[seedIndex].position[0] > stepSize_) ? seeds_[seedIndex].position[0] - stepSize_ : 0;
		int maxX = (seeds_[seedIndex].position[0] < width - stepSize_) ? seeds_[seedIndex].position[0] + stepSize_ : width;
		int minY = (seeds_[seedIndex].position[1] > stepSize_) ? seeds_[seedIndex].position[1] - stepSize_ : 0;
		int maxY = (seeds_[seedIndex].position[1] < height - stepSize_) ? seeds_[seedIndex].position[1] + stepSize_ : height;

		double seedX = seeds_[seedIndex].position[0];
		double seedY = seeds_[seedIndex].position[1];

		for (int y = minY; y < maxY; ++y) {
			for (int x = minX; x < maxX; ++x) {
				double distanceXY = (x - seedX)*(x - seedX) + (y - seedY)*(y - seedY);

				if (distanceXY < distancesToSeeds[width*y + x]) {
					distancesToSeeds[width*y + x] = distanceXY;
					labels_[width*y + x] = seedIndex;
				}
			}
		}
	}

	for (int y = 0; y < height; ++y) {
		for (int x = 0; x < width; ++x) {
			int pixelSeedIndex = labels_[width*y + x];
			seeds_[pixelSeedIndex].pixelTotal += 1;
			seeds_[pixelSeedIndex].colorSum[0] += firstLeftLabImage_(x, y, 0);
			seeds_[pixelSeedIndex].colorSum[1] += firstLeftLabImage_(x, y, 1);
			seeds_[pixelSeedIndex].colorSum[2] += firstLeftLabImage_(x, y, 2);
			seeds_[pixelSeedIndex].positionSum[0] += x;
			seeds_[pixelSeedIndex].positionSum[1] += y;
		}
	}
}

void Smoothfit::assignLabelWithConnectivity() {
	const double inlierThreshold = noDisparityPenalty_;

	int width = firstLeftLabImage_.width();
	int height = firstLeftLabImage_.height();
	//int seedTotal = static_cast<int>(seeds_.size());

	std::stack<int> boundaryPixels;
	rev::Image<unsigned char> boundaryFlagImage(width, height, 1);
	boundaryFlagImage.setTo(0);
	for (int y = 0; y < height; ++y) {
		for (int x = 0; x < width; ++x) {
			if (isBoundaryPixel(x, y)) {
				boundaryPixels.push(width*y + x);
				boundaryFlagImage(x, y) = 1;
			}
		}
	}

	while (!boundaryPixels.empty()) {
		int pixelIndex = boundaryPixels.top();
		boundaryPixels.pop();
		int x = pixelIndex%width;
		int y = pixelIndex/width;
		boundaryFlagImage(x, y) = 0;

		if (isInfeasible(x, y)) continue;

		int bestSeedIndex = -1;
		double bestEnergy = DBL_MAX;

		std::vector<int> neighborSeedIndices = getNeighborSeedIndices(x, y);
		for (int neighborIndex = 0; neighborIndex < static_cast<int>(neighborSeedIndices.size()); ++neighborIndex) {
			int seedIndex = neighborSeedIndices[neighborIndex];
			double pixelEnergy = computePixelEnergy(x, y, seedIndex);
			double boundaryLengthEnergy = computeBoundaryLengthEnergy(x, y, seedIndex);

			double assignmentEnergy = pixelEnergy + boundaryLengthEnergy;

			if (assignmentEnergy < bestEnergy) {
				bestSeedIndex = seedIndex;
				bestEnergy = assignmentEnergy;
			}
		}

		if (bestSeedIndex != labels_[width*y + x]) {
			int previousSeedIndex = labels_[width*y + x];
			labels_[width*y + x] = bestSeedIndex;

			double estimatedDisparity = seeds_[bestSeedIndex].disparityPlane[0]*x + seeds_[bestSeedIndex].disparityPlane[1]*y + seeds_[bestSeedIndex].disparityPlane[2];
			if (fabs(firstLeftDisparityImage_(x, y) - estimatedDisparity) <= inlierThreshold) outlierImage_(x, y) = 0;
			else outlierImage_(x, y) = 255;

			double pixelL = firstLeftLabImage_(x, y, 0);
			double pixelA = firstLeftLabImage_(x, y, 1);
			double pixelB = firstLeftLabImage_(x, y, 2);

			seeds_[previousSeedIndex].pixelTotal -= 1;
			seeds_[previousSeedIndex].colorSum[0] -= pixelL;
			seeds_[previousSeedIndex].colorSum[1] -= pixelA;
			seeds_[previousSeedIndex].colorSum[2] -= pixelB;
			seeds_[previousSeedIndex].positionSum[0] -= x;
			seeds_[previousSeedIndex].positionSum[1] -= y;

			seeds_[bestSeedIndex].pixelTotal += 1;
			seeds_[bestSeedIndex].colorSum[0] += pixelL;
			seeds_[bestSeedIndex].colorSum[1] += pixelA;
			seeds_[bestSeedIndex].colorSum[2] += pixelB;
			seeds_[bestSeedIndex].positionSum[0] += x;
			seeds_[bestSeedIndex].positionSum[1] += y;

			for (int neighorPixelIndex = 0; neighorPixelIndex < fourNeighborTotal; ++neighorPixelIndex) {
				int neighborX = x + fourNeighborOffsetX[neighorPixelIndex];
				if (neighborX < 0 || neighborX >= width) continue;
				int neighborY = y + fourNeighborOffsetY[neighorPixelIndex];
				if (neighborY < 0 || neighborY >= height) continue;

				if (boundaryFlagImage(neighborX, neighborY) > 0) continue;

				if (isBoundaryPixel(neighborX, neighborY)) {
					boundaryPixels.push(width*neighborY + neighborX);
				}
			}
		}
	}
}

bool Smoothfit::isBoundaryPixel(const int x, const int y) const {
	int width = firstLeftLabImage_.width();
	int height = firstLeftLabImage_.height();

	int pixelSeedIndex = labels_[width*y + x];
	for (int neighborIndex = 0; neighborIndex < fourNeighborTotal; ++neighborIndex) {
		int neighborX = x + fourNeighborOffsetX[neighborIndex];
		if (neighborX < 0 || neighborX >= width) continue;
		int neighborY = y + fourNeighborOffsetY[neighborIndex];
		if (neighborY < 0 || neighborY >= height) continue;

		if (labels_[width*neighborY + neighborX] != pixelSeedIndex) return true;
	}

	return false;
}

bool Smoothfit::isInfeasible(const int x, const int y) const {
	int width = firstLeftLabImage_.width();
	int height = firstLeftLabImage_.height();

	int pixelSeedIndex = labels_[width*y + x];

	int outcount = 0;
	for (int neighborIndex = 0; neighborIndex < eightNeighborTotal; ++neighborIndex) {
		int neighborX = x + eightNeighborOffsetX[neighborIndex];
		if (neighborX < 0 || neighborX >= width) continue;
		int neighborY = y + eightNeighborOffsetY[neighborIndex];
		if (neighborY < 0 || neighborY >= height) continue;

		if (labels_[width*neighborY + neighborX] != pixelSeedIndex) continue;

		int nextNeighborIndex = neighborIndex + 1;
		if (nextNeighborIndex >= eightNeighborTotal) nextNeighborIndex = 0;

		int nextX = x + eightNeighborOffsetX[nextNeighborIndex];
		if (nextX < 0 || nextX >= width) { ++outcount; continue; }
		int nextY = y + eightNeighborOffsetY[nextNeighborIndex];
		if (nextY < 0 || nextY >= height) { ++outcount; continue; }

		if (labels_[width*nextY + nextX] != pixelSeedIndex) ++outcount;
	}
	if (outcount > 1) return true;

	return false;
}

std::vector<int> Smoothfit::getNeighborSeedIndices(const int x, const int y) const {
	int width = firstLeftLabImage_.width();
	int height = firstLeftLabImage_.height();

	std::vector<int> neighborSeedIndices(1);
	neighborSeedIndices[0] = labels_[width*y + x];
	for (int neighborIndex = 0; neighborIndex < fourNeighborTotal; ++neighborIndex) {
		int neighborX = x + fourNeighborOffsetX[neighborIndex];
		if (neighborX < 0 || neighborX >= width) continue;
		int neighborY = y + fourNeighborOffsetY[neighborIndex];
		if (neighborY < 0 || neighborY >= height) continue;

		bool newSeedFlag = true;
		for (int listIndex = 0; listIndex < static_cast<int>(neighborSeedIndices.size()); ++listIndex) {
			if (labels_[width*neighborY + neighborX] == neighborSeedIndices[listIndex]) {
				newSeedFlag = false;
				break;
			}
		}

		if (newSeedFlag) {
			neighborSeedIndices.push_back(labels_[width*neighborY + neighborX]);
		}
	}

	return neighborSeedIndices;
}

double Smoothfit::computePixelEnergy(const int x, const int y, const int seedIndex) const {
	double positionWeight = compactnessWeight_/(stepSize_*stepSize_);

	double seedL = seeds_[seedIndex].colorSum[0]/seeds_[seedIndex].pixelTotal;
	double seedA = seeds_[seedIndex].colorSum[1]/seeds_[seedIndex].pixelTotal;
	double seedB = seeds_[seedIndex].colorSum[2]/seeds_[seedIndex].pixelTotal;
	double seedX = seeds_[seedIndex].positionSum[0]/seeds_[seedIndex].pixelTotal;
	double seedY = seeds_[seedIndex].positionSum[1]/seeds_[seedIndex].pixelTotal;
	double seedAlpha = seeds_[seedIndex].disparityPlane[0];
	double seedBeta = seeds_[seedIndex].disparityPlane[1];
	double seedGamma = seeds_[seedIndex].disparityPlane[2];

	double distanceFirstLeftLab = (firstLeftLabImage_(x, y, 0) - seedL)*(firstLeftLabImage_(x, y, 0) - seedL)
		+ (firstLeftLabImage_(x, y, 1) - seedA)*(firstLeftLabImage_(x, y, 1) - seedA)
		+ (firstLeftLabImage_(x, y, 2) - seedB)*(firstLeftLabImage_(x, y, 2) - seedB);
	double distanceXY = (x - seedX)*(x - seedX) + (y - seedY)*(y - seedY);

	// Default distances
	double distanceFirstLeftD = noDisparityPenalty_*noDisparityPenalty_;

	double estimatedDisparity = seedAlpha*x + seedBeta*y + seedGamma;
	if (estimatedDisparity > 0) {
		if (firstLeftDisparityImage_(x, y) > 0) {
			// distance of left disparities
			distanceFirstLeftD = (firstLeftDisparityImage_(x, y) - estimatedDisparity)*(firstLeftDisparityImage_(x, y) - estimatedDisparity);
		}
	}

	double distanceLab = distanceFirstLeftLab;
	double distanceD = distanceFirstLeftD;
	if (distanceD > noDisparityPenalty_*noDisparityPenalty_) distanceD = noDisparityPenalty_*noDisparityPenalty_;

	double distanceLabXYD = distanceLab + positionWeight*distanceXY + disparityWeight_*distanceD;

	return distanceLabXYD;
}

double Smoothfit::computeBoundaryLengthEnergy(const int x, const int y, const int seedIndex) const {
	const double lambdaBoundaryLength = 1000.0;

	int width = firstLeftLabImage_.width();
	int height = firstLeftLabImage_.height();

	int boundaryCount = 0;
	for (int neighborIndex = 0; neighborIndex < eightNeighborTotal; ++neighborIndex) {
		int neighborX = x + eightNeighborOffsetX[neighborIndex];
		if (neighborX < 0 || neighborX >= width) continue;
		int neighborY = y + eightNeighborOffsetY[neighborIndex];
		if (neighborY < 0 || neighborY >= height) continue;

		if (labels_[width*neighborY + neighborX] != seedIndex) ++boundaryCount;
	}

	return lambdaBoundaryLength*boundaryCount;
}

void writeBoundaryImage(const std::string& outputImageFilename,
                        const SegmentConfiguration& segmentConfiguration,
                        const std::vector<int>& boundaryVariables)
{
    const rev::Image<unsigned short>& segmentIndexImage = segmentConfiguration.segmentIndexImage();
    int width = segmentIndexImage.width();
    int height = segmentIndexImage.height();

    rev::Image<unsigned char> boundaryImage(width, height, 3);
    boundaryImage.setTo(rev::Color<unsigned char>(255,255,255));
    for (int boundaryIndex = 0; boundaryIndex < segmentConfiguration.boundaryTotal(); ++boundaryIndex) {
        const SegmentBoundary& boundary = segmentConfiguration.boundary(boundaryIndex);
        for (int pixelIndex = 0; pixelIndex < boundary.boundaryPixelTotal(); ++pixelIndex) {
            double pixelX = boundary.boundaryPixelX(pixelIndex);
            double pixelY = boundary.boundaryPixelY(pixelIndex);

            int px = static_cast<int>(pixelX);
            int py = static_cast<int>(pixelY);

            if (pixelX - px > 0) {
                if (boundaryVariables[boundaryIndex] == 2) {
					if (px - 1 >= 0) boundaryImage.setColor(px-1, py, rev::Color<unsigned char>(0,255,0));
                    boundaryImage.setColor(px, py, rev::Color<unsigned char>(0,255,0));
                    boundaryImage.setColor(px+1, py, rev::Color<unsigned char>(0,255,0));
					if (px+2 < width) boundaryImage.setColor(px+1, py, rev::Color<unsigned char>(0,255,0));
                } else if (boundaryVariables[boundaryIndex] == 3) {
					//if (px - 1 >= 0) boundaryImage.setColor(px-1, py, rev::Color<unsigned char>(128,128,128));
                    boundaryImage.setColor(px, py, rev::Color<unsigned char>(128,128,128));
                    //boundaryImage.setColor(px+1, py, rev::Color<unsigned char>(128,128,128));
                } else {
                    if (segmentIndexImage(px, py) == boundary.segmentIndex(boundaryVariables[boundaryIndex])) {
						if (px - 1 >= 0) boundaryImage.setColor(px-1, py, rev::Color<unsigned char>(255,0,0));
                        boundaryImage.setColor(px, py, rev::Color<unsigned char>(255,0,0));
                        boundaryImage.setColor(px+1, py, rev::Color<unsigned char>(0,0,255));
						if (px+2 < width) boundaryImage.setColor(px+1, py, rev::Color<unsigned char>(0,0,255));
                    } else {
						if (px - 1 >= 0) boundaryImage.setColor(px-1, py, rev::Color<unsigned char>(0,0,255));
                        boundaryImage.setColor(px, py, rev::Color<unsigned char>(0,0,255));
                        boundaryImage.setColor(px+1, py, rev::Color<unsigned char>(255,0,0));
						if (px+2 < width) boundaryImage.setColor(px+1, py, rev::Color<unsigned char>(255,0,0));
                    }
                }
            } else {
                if (boundaryVariables[boundaryIndex] == 2) {
					if (py - 1 >= 0) boundaryImage.setColor(px, py-1, rev::Color<unsigned char>(0,255,0));
                    boundaryImage.setColor(px, py, rev::Color<unsigned char>(0,255,0));
                    boundaryImage.setColor(px, py+1, rev::Color<unsigned char>(0,255,0));
					if (py+2 < height) boundaryImage.setColor(px, py+2, rev::Color<unsigned char>(0,255,0));
                } else if (boundaryVariables[boundaryIndex] == 3) {
                    boundaryImage.setColor(px, py, rev::Color<unsigned char>(128,128,128));
                    //boundaryImage.setColor(px, py+1, rev::Color<unsigned char>(128,128,128));
                } else {
                    if (segmentIndexImage(px, py) == boundary.segmentIndex(boundaryVariables[boundaryIndex])) {
						if (py - 1 >= 0) boundaryImage.setColor(px, py-1, rev::Color<unsigned char>(255,0,0));
						boundaryImage.setColor(px, py, rev::Color<unsigned char>(255,0,0));
                        boundaryImage.setColor(px, py+1, rev::Color<unsigned char>(0,0,255));
						if (py+2 < height) boundaryImage.setColor(px, py+2, rev::Color<unsigned char>(0,0,255));
                    } else {
						if (py - 1 >= 0) boundaryImage.setColor(px, py-1, rev::Color<unsigned char>(0,0,255));
						boundaryImage.setColor(px, py, rev::Color<unsigned char>(0,0,255));
                        boundaryImage.setColor(px, py+1, rev::Color<unsigned char>(255,0,0));
						if (py+2 < height) boundaryImage.setColor(px, py+2, rev::Color<unsigned char>(255,0,0));
                    }
                }
            }
        }

    }
    rev::writeImageFile(outputImageFilename, boundaryImage);
}

void Smoothfit::updateSeeds() {
	int width = firstLeftLabImage_.width();
	int height = firstLeftLabImage_.height();
	rev::Image<unsigned short> currentSegmentImage(width, height, 1);
	for (int y = 0; y < height; ++y) {
		for (int x = 0; x < width; ++x) {
			currentSegmentImage(x, y) = labels_[width*y + x];
		}
	}

	SegmentConfiguration segmentConfiguration;
	segmentConfiguration.buildWithoutSupportPoint(currentSegmentImage);

	std::vector< std::vector<double> > disparityPlaneParameters;
	if (initialFlag_) {
		estimateInitialFit(segmentConfiguration, disparityPlaneParameters, outlierImage_);
		initialFlag_ = false;
	} else {
		estimateOutlierImage(segmentConfiguration, disparityPlaneParameters, outlierImage_);
	}

	std::vector<bool> updateFlags(segmentConfiguration.segmentTotal(), true);
	std::vector<int> boundaryLabels;
	for (int i = 0; i < 10; ++i) {
		estimateBoundaryParameters(segmentConfiguration, disparityPlaneParameters, boundaryLabels);
		estimateSmoothFit(segmentConfiguration, boundaryLabels, outlierImage_, disparityPlaneParameters, updateFlags);
	}

	makeBoundaryLabelImage(segmentConfiguration, boundaryLabels);
	makeBoundaryLabelList(segmentConfiguration, boundaryLabels);

	for (int i = 0; i < labelTotal_; ++i) {
		seeds_[i].disparityPlane[0] = disparityPlaneParameters[i][0];
		seeds_[i].disparityPlane[1] = disparityPlaneParameters[i][1];
		seeds_[i].disparityPlane[2] = disparityPlaneParameters[i][2];
	}
}

void Smoothfit::updateSeedsColorAndPosition() {
	int width = firstLeftLabImage_.width();
	int height = firstLeftLabImage_.height();
	int seedTotal = static_cast<int>(seeds_.size());

	std::vector<int> segmentSizes(seedTotal, 0);
	std::vector<LabXYD> segmentSigmas(seedTotal);
	for (int y = 0; y < height; ++y) {
		for (int x = 0; x < width; ++x) {
			int pixelSeedIndex = labels_[width*y + x];
			if (pixelSeedIndex >= 0) {
				segmentSigmas[pixelSeedIndex].color[0] += firstLeftLabImage_(x, y, 0);
				segmentSigmas[pixelSeedIndex].color[1] += firstLeftLabImage_(x, y, 1);
				segmentSigmas[pixelSeedIndex].color[2] += firstLeftLabImage_(x, y, 2);
				segmentSigmas[pixelSeedIndex].position[0] += x;
				segmentSigmas[pixelSeedIndex].position[1] += y;
				++segmentSizes[pixelSeedIndex];
			}
		}
	}

	for (int seedIndex = 0; seedIndex < seedTotal; ++seedIndex) {
		double segmentSizeInverse = 1.0;
		if (segmentSizes[seedIndex] > 0) segmentSizeInverse = 1.0/segmentSizes[seedIndex];

		seeds_[seedIndex].color[0] = segmentSigmas[seedIndex].color[0]*segmentSizeInverse;
		seeds_[seedIndex].color[1] = segmentSigmas[seedIndex].color[1]*segmentSizeInverse;
		seeds_[seedIndex].color[2] = segmentSigmas[seedIndex].color[2]*segmentSizeInverse;
		seeds_[seedIndex].position[0] = segmentSigmas[seedIndex].position[0]*segmentSizeInverse;
		seeds_[seedIndex].position[1] = segmentSigmas[seedIndex].position[1]*segmentSizeInverse;
	}
}

void Smoothfit::estimateDisparityPlaneParameter() {
	std::vector< std::vector<DisparityPixel> > segmentDisparityPixels = makeDisparityPixelList();
	int seedTotal = static_cast<int>(segmentDisparityPixels.size());

	for (int seedIndex = 0; seedIndex < seedTotal; ++seedIndex) {
		std::vector<double> planeParameter = estimateDisparityPlaneParameter(segmentDisparityPixels[seedIndex]);

		seeds_[seedIndex].disparityPlane[0] = planeParameter[0];
		seeds_[seedIndex].disparityPlane[1] = planeParameter[1];
		seeds_[seedIndex].disparityPlane[2] = planeParameter[2];
	}
}

std::vector< std::vector<Smoothfit::DisparityPixel> > Smoothfit::makeDisparityPixelList() const {
	int width = firstLeftDisparityImage_.width();
	int height = firstLeftDisparityImage_.height();
	int seedTotal = static_cast<int>(seeds_.size());

	std::vector< std::vector<DisparityPixel> > segmentDisparityPixels(seedTotal);
	for (int y = 0; y < height; ++y) {
		for (int x = 0; x < width; ++x) {
			if (firstLeftDisparityImage_(x, y) > 0) {
				int pixelSeedIndex = labels_[width*y + x];
				if (pixelSeedIndex >= 0) {
					DisparityPixel newDisparityPixel;
					newDisparityPixel.x = x;
					newDisparityPixel.y = y;
					newDisparityPixel.d = firstLeftDisparityImage_(x, y);
					segmentDisparityPixels[pixelSeedIndex].push_back(newDisparityPixel);
				}
			}
		}
	}

	return segmentDisparityPixels;
}

std::vector<double> Smoothfit::estimateDisparityPlaneParameter(const std::vector<DisparityPixel>& disparityPixels) const {
	std::vector<double> planeParameter(3);

	int pixelTotal = static_cast<int>(disparityPixels.size());
	if (pixelTotal < 3) {
		planeParameter[0] = 0;
		planeParameter[1] = 0;
		planeParameter[2] = -1;
	} else {
		bool isSameX = true;
		bool isSameY = true;
		for (int pixelIndex = 1; pixelIndex < pixelTotal; ++pixelIndex) {
			if (disparityPixels[pixelIndex].x != disparityPixels[0].x) isSameX = false;
			if (disparityPixels[pixelIndex].y != disparityPixels[0].y) isSameY = false;
			if (!isSameX && !isSameY) break;
		}
		if (isSameX || isSameY) {
			double disparitySum = 0.0;
			for (int pixelIndex = 0; pixelIndex < pixelTotal; ++pixelIndex) disparitySum += disparityPixels[pixelIndex].d;

			planeParameter[0] = 0;
			planeParameter[1] = 0;
			planeParameter[2] = -1;
		} else {
			planeParameter = estimateDisparityPlaneParameterRANSAC(disparityPixels);
		}
	}

	return planeParameter;
}

std::vector<double> Smoothfit::estimateDisparityPlaneParameterRANSAC(const std::vector<DisparityPixel>& disparityPixels) const {
	const double inlierThreshold = 2.0;
	const double confidenceLevel = 0.99;

	int pixelTotal = static_cast<int>(disparityPixels.size());

	int samplingTotal = pixelTotal*2;

	int bestInlierTotal = 0;
	std::vector<bool> bestInlierFlags(pixelTotal);
	int samplingCount = 0;
	while (samplingCount < samplingTotal) {
		// Randomly select 3 pixels
		int drawIndices[3];
		drawIndices[0] = rand()%pixelTotal;
		drawIndices[1] = rand()%pixelTotal;
		while(drawIndices[1] == drawIndices[0]) drawIndices[1] = rand()%pixelTotal;
		drawIndices[2] = rand()%pixelTotal;
		while(drawIndices[2] == drawIndices[1] || drawIndices[2] == drawIndices[0]
		|| (disparityPixels[drawIndices[0]].x == disparityPixels[drawIndices[1]].x && disparityPixels[drawIndices[0]].x == disparityPixels[drawIndices[2]].x)
			|| (disparityPixels[drawIndices[0]].y == disparityPixels[drawIndices[1]].y && disparityPixels[drawIndices[0]].y == disparityPixels[drawIndices[2]].y))
		{
			drawIndices[2] = rand()%pixelTotal;
		}

		// Compute plane parameters
        Eigen::Matrix3d matPosition;
        Eigen::Vector3d vecDisparity;
        for (int i = 0; i < 3; ++i) {
            matPosition(i, 0) = disparityPixels[drawIndices[i]].x;
            matPosition(i, 1) = disparityPixels[drawIndices[i]].y;
            matPosition(i, 2) = 1.0;
            vecDisparity(i) = disparityPixels[drawIndices[i]].d;
        }
        Eigen::Vector3d planeParameter = matPosition.colPivHouseholderQr().solve(vecDisparity);

		// Count the number of inliers
		int inlierTotal = 0;
		std::vector<bool> inlierFlags(pixelTotal);
		for (int pixelIndex = 0; pixelIndex < pixelTotal; ++pixelIndex) {
			double estimatedDisparity = planeParameter[0]*disparityPixels[pixelIndex].x
				+ planeParameter[1]*disparityPixels[pixelIndex].y
				+ planeParameter[2];
			if (fabs(estimatedDisparity - disparityPixels[pixelIndex].d) <= inlierThreshold) {
				++inlierTotal;
				inlierFlags[pixelIndex] = true;
			} else {
				inlierFlags[pixelIndex] = false;
			}
		}

		// Update best inliers
		if (inlierTotal > bestInlierTotal) {
			bestInlierTotal = inlierTotal;
			bestInlierFlags = inlierFlags;

			samplingTotal = computeRequiredSamplingTotal(3, bestInlierTotal, pixelTotal, samplingTotal, confidenceLevel);
		}

		// Increment
		++samplingCount;
	}

    Eigen::MatrixXd matPosition(bestInlierTotal, 3);
    Eigen::VectorXd vecDisparity(bestInlierTotal);
    int inlierIndex = 0;
    for (int pixelIndex = 0; pixelIndex < pixelTotal; ++pixelIndex) {
        if (bestInlierFlags[pixelIndex]) {
            matPosition(inlierIndex, 0) = disparityPixels[pixelIndex].x;
            matPosition(inlierIndex, 1) = disparityPixels[pixelIndex].y;
            matPosition(inlierIndex, 2) = 1.0;
            vecDisparity(inlierIndex) = disparityPixels[pixelIndex].d;
            ++inlierIndex;
        }
    }
    Eigen::Vector3d vecPlaneParameter = matPosition.colPivHouseholderQr().solve(vecDisparity);
    std::vector<double> planeParameter(3);
    planeParameter[0] = vecPlaneParameter(0);
    planeParameter[1] = vecPlaneParameter(1);
    planeParameter[2] = vecPlaneParameter(2);

	return planeParameter;
}

void Smoothfit::solvePlaneEquations(const double x1, const double y1, const double z1, const double d1,
									 const double x2, const double y2, const double z2, const double d2,
									 const double x3, const double y3, const double z3, const double d3,
									 std::vector<double>& planeParameter) const
{
	const double epsilonValue = 1e-10;

	planeParameter.resize(3);

	double denominatorA = (x1*z2 - x2*z1)*(y2*z3 - y3*z2) - (x2*z3 - x3*z2)*(y1*z2 - y2*z1);
	if (denominatorA < epsilonValue) {
		planeParameter[0] = 0.0;
		planeParameter[1] = 0.0;
		planeParameter[2] = -1.0;
		return;
	}

	planeParameter[0] = ((z2*d1 - z1*d2)*(y2*z3 - y3*z2) - (z3*d2 - z2*d3)*(y1*z2 - y2*z1))/denominatorA;

	double denominatorB = y1*z2 - y2*z1;
	if (denominatorB > epsilonValue) {
		planeParameter[1] = (z2*d1 - z1*d2 - planeParameter[0]*(x1*z2 - x2*z1))/denominatorB;
	} else {
		denominatorB = y2*z3 - y3*z2;
		planeParameter[1] = (z3*d2 - z2*d3 - planeParameter[0]*(x2*z3 - x3*z2))/denominatorB;
	}
	if (z1 > epsilonValue) {
		planeParameter[2] = (d1 - planeParameter[0]*x1 - planeParameter[1]*y1)/z1;
	} else if (z2 > epsilonValue) {
		planeParameter[2] = (d2 - planeParameter[0]*x2 - planeParameter[1]*y2)/z2;
	} else {
		planeParameter[2] = (d3 - planeParameter[0]*x3 - planeParameter[1]*y3)/z3;
	}
}

void Smoothfit::labelConnectedPixels(const int x, const int y, const int newLabelIndex,
									  std::vector<int>& newLabels, std::vector<int>& connectedXs, std::vector<int>& connectedYs) const
{
	int width = firstLeftLabImage_.width();
	int height = firstLeftLabImage_.height();


	int originalLabelIndex = labels_[width*y + x];
	for (int neighborIndex = 0; neighborIndex < fourNeighborTotal; ++neighborIndex) {
		int neighborX = x + fourNeighborOffsetX[neighborIndex];
		int neighborY = y + fourNeighborOffsetY[neighborIndex];
		if (neighborX < 0 || neighborX >= width || neighborY < 0 || neighborY >= height) continue;

		if (newLabels[width*neighborY + neighborX] < 0 && labels_[width*neighborY + neighborX] == originalLabelIndex) {
			connectedXs.push_back(neighborX);
			connectedYs.push_back(neighborY);
			newLabels[width*neighborY + neighborX] = newLabelIndex;
			labelConnectedPixels(neighborX, neighborY, newLabelIndex, newLabels, connectedXs, connectedYs);
		}
	}
}

void Smoothfit::subpixelLabImageIntensity(const rev::Image<float>& labImage,
											  const double x, const double y,
											  double& subpixelL, double& subpixelA, double& subpixelB) const
{
	int x0 = static_cast<int>(floor(x));
	if (x0 < 0) x0 = 0;
	int y0 = static_cast<int>(floor(y));
	if (y0 < 0) y0 = 0;
	int x1 = x0 + 1;
	if (x1 >= labImage.width()) x1 = labImage.width() - 1;
	int y1 = y0 + 1;
	if (y1 >= labImage.height()) y1 = labImage.height() - 1;

	float l00 = labImage(x0, y0, 0);
	float l01 = labImage(x0, y1, 0);
	float l10 = labImage(x1, y0, 0);
	float l11 = labImage(x1, y1, 0);
	float a00 = labImage(x0, y0, 1);
	float a01 = labImage(x0, y1, 1);
	float a10 = labImage(x1, y0, 1);
	float a11 = labImage(x1, y1, 1);
	float b00 = labImage(x0, y0, 2);
	float b01 = labImage(x0, y1, 2);
	float b10 = labImage(x1, y0, 2);
	float b11 = labImage(x1, y1, 2);

	double rx = x - x0;
	double ry = y - y0;

	subpixelL = (1.0 - rx)*(1.0 - ry)*l00 + (1.0 - rx)*ry*l01 + rx*(1.0 - ry)*l10 + rx*ry*l11;
	subpixelA = (1.0 - rx)*(1.0 - ry)*a00 + (1.0 - rx)*ry*a01 + rx*(1.0 - ry)*a10 + rx*ry*a11;
	subpixelB = (1.0 - rx)*(1.0 - ry)*b00 + (1.0 - rx)*ry*b01 + rx*(1.0 - ry)*b10 + rx*ry*b11;
}

double Smoothfit::subpixelDisparityImageIntensity(const rev::Image<float>& disparityImage,
													  const double x, const double y) const
{
	int x0 = static_cast<int>(floor(x));
	if (x0 < 0) x0 = 0;
	int y0 = static_cast<int>(floor(y));
	if (y0 < 0) y0 = 0;
	int x1 = x0 + 1;
	if (x1 >= disparityImage.width()) x1 = disparityImage.width() - 1;
	int y1 = y0 + 1;
	if (y1 >= disparityImage.height()) y1 = disparityImage.height() - 1;

	float pix00 = disparityImage(x0, y0);
	float pix01 = disparityImage(x0, y1);
	float pix10 = disparityImage(x1, y0);
	float pix11 = disparityImage(x1, y1);

	if (pix00 == 0) pix00 = pix10;
	if (pix10 == 0) pix10 = pix00;
	if (pix01 == 0) pix01 = pix11;
	if (pix11 == 0) pix11 = pix01;

	double rx = x - x0;
	double ry = y - y0;

	double pixelValue = (1.0 - rx)*(1.0 - ry)*pix00 + (1.0 - rx)*ry*pix01 + rx*(1.0 - ry)*pix10 + rx*ry*pix11;

	return pixelValue;
}

void Smoothfit::estimateInitialFit(const SegmentConfiguration& segmentConfiguration,
									   std::vector< std::vector<double> >& disparityPlaneParameters,
									   rev::Image<unsigned char>& outlierImage) const
{
	const double confidenceLevel = 0.99;
	const double inlierThreshold = 1.0;

	int segmentTotal = segmentConfiguration.segmentTotal();
	disparityPlaneParameters.resize(segmentTotal);

	outlierImage.setTo(0);

	for (int segmentIndex = 0; segmentIndex < segmentTotal; ++segmentIndex) {
		Segment segment = segmentConfiguration.segment(segmentIndex);

		if (segment.segmentPixelTotal() < 3) {
			double dispSum = 0;
			int dispPixTotal = 0;
			for (int pixelIndex = 0; pixelIndex < segment.segmentPixelTotal(); ++pixelIndex) {
				int pixelX = segment.segmentPixelX(pixelIndex);
				int pixelY = segment.segmentPixelY(pixelIndex);
				if (firstLeftDisparityImage_(pixelX, pixelY) == 0) continue;

				++dispPixTotal;
				dispSum += firstLeftDisparityImage_(pixelX, pixelY);
			}

			disparityPlaneParameters[segmentIndex].resize(3);
			if (dispPixTotal == 0) {
				disparityPlaneParameters[segmentIndex][0] = 0;
				disparityPlaneParameters[segmentIndex][1] = 0;
				disparityPlaneParameters[segmentIndex][2] = 0;
			} else {
				double aveDisp = dispSum/dispPixTotal;
				disparityPlaneParameters[segmentIndex][0] = 0;
				disparityPlaneParameters[segmentIndex][1] = 0;
				disparityPlaneParameters[segmentIndex][2] = aveDisp;
			}
			continue;
		}

		std::vector<DisparityPixelS> disparityPixels;
		for (int pixelIndex = 0; pixelIndex < segment.segmentPixelTotal(); ++pixelIndex) {
			int pixelX = segment.segmentPixelX(pixelIndex);
			int pixelY = segment.segmentPixelY(pixelIndex);
			if (firstLeftDisparityImage_(pixelX, pixelY) == 0) continue;

			DisparityPixelS newDisparityPixel;
			newDisparityPixel.normalizedX = pixelX;// - segment.centerX();
			newDisparityPixel.normalizedY = pixelY;// - segment.centerY();
			newDisparityPixel.disparity = firstLeftDisparityImage_(pixelX, pixelY);
			disparityPixels.push_back(newDisparityPixel);
		}
		int disparityPixelTotal = static_cast<int>(disparityPixels.size());

		disparityPlaneParameters[segmentIndex].resize(3);
		if (disparityPixelTotal < segment.segmentPixelTotal()/10.0 || disparityPixelTotal < 3) {
			disparityPlaneParameters[segmentIndex][0] = 0.0;
			disparityPlaneParameters[segmentIndex][1] = 0.0;
			disparityPlaneParameters[segmentIndex][2] = 0.0;
		} else {
			int bestInlierTotal = 0;
			std::vector<bool> bestInlierFlags(disparityPixelTotal);
			int samplingTotal = disparityPixelTotal;
			int samplingCount = 0;
			while (samplingCount < samplingTotal) {
				int drawIndices[3];
				drawIndices[0] = rand()%disparityPixelTotal;
				drawIndices[1] = rand()%disparityPixelTotal;
				while (drawIndices[1] == drawIndices[0]) drawIndices[1] = rand()%disparityPixelTotal;
				drawIndices[2] = rand()%disparityPixelTotal;
				while (drawIndices[2] == drawIndices[0] || drawIndices[2] == drawIndices[1]) drawIndices[2] = rand()%disparityPixelTotal;

				Eigen::Matrix3d matPosition;
				Eigen::Vector3d vecDisparity;
				for (int i = 0; i < 3; i++) {
					matPosition(i, 0) = disparityPixels[drawIndices[i]].normalizedX;
					matPosition(i, 1) = disparityPixels[drawIndices[i]].normalizedY;
					matPosition(i, 2) = 1.0;
					vecDisparity(i) = disparityPixels[drawIndices[i]].disparity;
				}
				Eigen::Vector3d param = matPosition.colPivHouseholderQr().solve(vecDisparity);

				// Count the number of inliers
				int inlierTotal = 0;
				std::vector<bool> inlierFlags(disparityPixelTotal);
				for (int i = 0; i < disparityPixelTotal; ++i) {
					double estimateDisparity = param(0)*disparityPixels[i].normalizedX + param(1)*disparityPixels[i].normalizedY + param(2);
					if (fabs(estimateDisparity - disparityPixels[i].disparity) <= inlierThreshold) {
						++inlierTotal;
						inlierFlags[i] = true;
					} else {
						inlierFlags[i] = false;
					}
				}

				if (inlierTotal > bestInlierTotal) {
					bestInlierTotal = inlierTotal;
					bestInlierFlags = inlierFlags;

					samplingTotal = computeRequiredSamplingTotal(3, bestInlierTotal, disparityPixelTotal, samplingTotal, confidenceLevel);
				}

				++samplingCount;
			}

			Eigen::MatrixXd matPosition(bestInlierTotal, 3);
			Eigen::VectorXd vecDisparity(bestInlierTotal);
			int inlierIndex = 0;
			for (int i = 0; i < disparityPixelTotal; ++i) {
				if (bestInlierFlags[i]) {
					matPosition(inlierIndex, 0) = disparityPixels[i].normalizedX;
					matPosition(inlierIndex, 1) = disparityPixels[i].normalizedY;
					matPosition(inlierIndex, 2) = 1.0;
					vecDisparity(inlierIndex) = disparityPixels[i].disparity;
					++inlierIndex;
				}
			}
			Eigen::Vector3d vecPlaneParameter = matPosition.colPivHouseholderQr().solve(vecDisparity);
			disparityPlaneParameters[segmentIndex][0] = vecPlaneParameter(0);
			disparityPlaneParameters[segmentIndex][1] = vecPlaneParameter(1);
			disparityPlaneParameters[segmentIndex][2] = vecPlaneParameter(2);

			for (int i = 0; i < disparityPixelTotal; ++i) {
				if (!bestInlierFlags[i]) {
					outlierImage(disparityPixels[i].normalizedX, disparityPixels[i].normalizedY) = 255;
				}
			}
		}
	}

	interpolatePlaneParameters(segmentConfiguration, disparityPlaneParameters);
}

void Smoothfit::estimateBoundaryParameters(const SegmentConfiguration& segmentConfiguration,
											   const std::vector< std::vector<double> >& disparityPlaneParameters,
											   std::vector<int>& boundaryVariables) const
{
	const double impossiblePenalty = 30.0;
	const double occlusionPenalty = 15.0;
	const double hingePenalty = 5.0;

	int boundaryTotal = segmentConfiguration.boundaryTotal();
	boundaryVariables.resize(boundaryTotal, 3);

	for (int boundaryIndex = 0; boundaryIndex < boundaryTotal; ++boundaryIndex) {
		// Boundary energies
		SegmentBoundary currentBoundary = segmentConfiguration.boundary(boundaryIndex);
		int firstSegmentIndex = currentBoundary.segmentIndex(0);
		int secondSegmentIndex = currentBoundary.segmentIndex(1);
		Segment firstSegment = segmentConfiguration.segment(firstSegmentIndex);
		Segment secondSegment = segmentConfiguration.segment(secondSegmentIndex);

		std::vector<double> boundaryEnergies(4);

		// Hinge
		double hingeSquaredError = 0;
		double hingeError = 0;
		for (int boundaryPixelIndex = 0; boundaryPixelIndex < currentBoundary.boundaryPixelTotal(); ++boundaryPixelIndex) {
			double pixelX = currentBoundary.boundaryPixelX(boundaryPixelIndex);
			double pixelY = currentBoundary.boundaryPixelY(boundaryPixelIndex);

			double firstDisparity = disparityPlaneParameters[firstSegmentIndex][0]*pixelX
				+ disparityPlaneParameters[firstSegmentIndex][1]*pixelY
				+ disparityPlaneParameters[firstSegmentIndex][2];
			double secondDisparity = disparityPlaneParameters[secondSegmentIndex][0]*pixelX
				+ disparityPlaneParameters[secondSegmentIndex][1]*pixelY
				+ disparityPlaneParameters[secondSegmentIndex][2];

			hingeError += firstDisparity - secondDisparity;
			hingeSquaredError += (firstDisparity - secondDisparity)*(firstDisparity - secondDisparity);
		}
		hingeSquaredError /= currentBoundary.boundaryPixelTotal();
		boundaryEnergies[2] = hingePenalty + hingeSquaredError;

		// Occlusion
		if (hingeError > 0) { boundaryEnergies[0] = occlusionPenalty; boundaryEnergies[1] = occlusionPenalty + impossiblePenalty; }
		else { boundaryEnergies[0] = occlusionPenalty + impossiblePenalty; boundaryEnergies[1] = occlusionPenalty; }

		// Coplanar
		double coplanarSquaredError = 0;
		for (int firstPixelIndex = 0; firstPixelIndex < firstSegment.segmentPixelTotal(); ++firstPixelIndex) {
			int pixelX = firstSegment.segmentPixelX(firstPixelIndex);
			int pixelY = firstSegment.segmentPixelY(firstPixelIndex);

			double firstDisparity = disparityPlaneParameters[firstSegmentIndex][0]*pixelX
				+ disparityPlaneParameters[firstSegmentIndex][1]*pixelY
				+ disparityPlaneParameters[firstSegmentIndex][2];
			double secondDisparity = disparityPlaneParameters[secondSegmentIndex][0]*pixelX
				+ disparityPlaneParameters[secondSegmentIndex][1]*pixelY
				+ disparityPlaneParameters[secondSegmentIndex][2];
			coplanarSquaredError += (firstDisparity - secondDisparity)*(firstDisparity - secondDisparity);
		}
		for (int secondPixelIndex = 0; secondPixelIndex < secondSegment.segmentPixelTotal(); ++secondPixelIndex) {
			int pixelX = secondSegment.segmentPixelX(secondPixelIndex);
			int pixelY = secondSegment.segmentPixelY(secondPixelIndex);

			double firstDisparity = disparityPlaneParameters[firstSegmentIndex][0]*pixelX
				+ disparityPlaneParameters[firstSegmentIndex][1]*pixelY
				+ disparityPlaneParameters[firstSegmentIndex][2];
			double secondDisparity = disparityPlaneParameters[secondSegmentIndex][0]*pixelX
				+ disparityPlaneParameters[secondSegmentIndex][1]*pixelY
				+ disparityPlaneParameters[secondSegmentIndex][2];
			coplanarSquaredError += (firstDisparity - secondDisparity)*(firstDisparity - secondDisparity);
		}
		coplanarSquaredError /= (firstSegment.segmentPixelTotal() + secondSegment.segmentPixelTotal());
		boundaryEnergies[3] = coplanarSquaredError;

		int minBoundaryLabel = 0;
		if (boundaryEnergies[1] < boundaryEnergies[minBoundaryLabel]) minBoundaryLabel = 1;
		if (boundaryEnergies[2] < boundaryEnergies[minBoundaryLabel]) minBoundaryLabel = 2;
		if (boundaryEnergies[3] < boundaryEnergies[minBoundaryLabel]) minBoundaryLabel = 3;

		boundaryVariables[boundaryIndex] = minBoundaryLabel;
	}
}

void Smoothfit::estimateSmoothFit(const SegmentConfiguration& segmentConfiguration,
									  const std::vector<int>& boundaryVariables,
									  const rev::Image<unsigned char>& outlierImage,
									  std::vector< std::vector<double> >& disparityPlaneParameters,
									  std::vector<bool>& updateFlags) const
{
	const double weightCoplanar = 0.2;
	const double weightHinge = 0.2;
	const double updateThreshold = 1e-3;
	const double updateThresholdSquare = updateThreshold*updateThreshold;

	int segmentTotal = segmentConfiguration.segmentTotal();
	std::vector<bool> newUpdateFlags(segmentTotal, false);
	for (int segmentIndex = 0; segmentIndex < segmentTotal; ++segmentIndex) {
		//if (!updateFlags[segmentIndex]) continue;

		std::vector<double> previousParameters = disparityPlaneParameters[segmentIndex];

		Eigen::Matrix3d matA;
		matA.setZero();
		Eigen::Vector3d vecB;
		vecB.setZero();

		Segment segment = segmentConfiguration.segment(segmentIndex);
		int segmentPixelTotal = segment.segmentPixelTotal();
		int dataPointTotal = 0;
		double weightData = 1.0;///segmentPixelTotal;
		for (int pixelIndex = 0; pixelIndex < segmentPixelTotal; ++pixelIndex) {
			int pixelX = segment.segmentPixelX(pixelIndex);
			int pixelY = segment.segmentPixelY(pixelIndex);
			if (firstLeftDisparityImage_(pixelX, pixelY) == 0) continue;
			if (outlierImage(pixelX, pixelY) > 0) continue;

			matA(0, 0) += weightData*pixelX*pixelX;
			matA(0, 1) += weightData*pixelX*pixelY;
			matA(0, 2) += weightData*pixelX;
			matA(1, 0) += weightData*pixelX*pixelY;
			matA(1, 1) += weightData*pixelY*pixelY;
			matA(1, 2) += weightData*pixelY;
			matA(2, 0) += weightData*pixelX;
			matA(2, 1) += weightData*pixelY;
			matA(2, 2) += weightData*1.0;
			vecB(0) += weightData*firstLeftDisparityImage_(pixelX, pixelY)*pixelX;
			vecB(1) += weightData*firstLeftDisparityImage_(pixelX, pixelY)*pixelY;
			vecB(2) += weightData*firstLeftDisparityImage_(pixelX, pixelY);

			++dataPointTotal;
		}

		for (int neighborIndex = 0; neighborIndex < segment.boundaryTotal(); ++neighborIndex) {
			int neighborBoundaryIndex = segment.boundaryIndex(neighborIndex);
			if (boundaryVariables[neighborBoundaryIndex] < 2) continue;

			int neighborSegmentIndex = segmentConfiguration.boundary(neighborBoundaryIndex).segmentIndex(0);
			if (neighborSegmentIndex == segmentIndex) neighborSegmentIndex = segmentConfiguration.boundary(neighborBoundaryIndex).segmentIndex(1);

			if (boundaryVariables[neighborBoundaryIndex] == 2) {
				int boundaryPointTotal = segmentConfiguration.boundary(neighborBoundaryIndex).boundaryPixelTotal();
				double weightCoef = weightHinge/boundaryPointTotal*stepSize_*stepSize_;
				for (int boundaryPointIndex = 0; boundaryPointIndex < boundaryPointTotal; ++boundaryPointIndex) {
					double boundaryPointX = segmentConfiguration.boundary(neighborBoundaryIndex).boundaryPixelX(boundaryPointIndex);
					double boundaryPointY = segmentConfiguration.boundary(neighborBoundaryIndex).boundaryPixelY(boundaryPointIndex);
					double boundaryDisparity = disparityPlaneParameters[neighborSegmentIndex][0]*boundaryPointX
						+ disparityPlaneParameters[neighborSegmentIndex][1]*boundaryPointY
						+ disparityPlaneParameters[neighborSegmentIndex][2];

					matA(0, 0) += weightCoef*boundaryPointX*boundaryPointX;
					matA(0, 1) += weightCoef*boundaryPointX*boundaryPointY;
					matA(0, 2) += weightCoef*boundaryPointX;
					matA(1, 0) += weightCoef*boundaryPointX*boundaryPointY;
					matA(1, 1) += weightCoef*boundaryPointY*boundaryPointY;
					matA(1, 2) += weightCoef*boundaryPointY;
					matA(2, 0) += weightCoef*boundaryPointX;
					matA(2, 1) += weightCoef*boundaryPointY;
					matA(2, 2) += weightCoef*1.0;
					vecB(0) += weightCoef*boundaryDisparity*boundaryPointX;
					vecB(1) += weightCoef*boundaryDisparity*boundaryPointY;
					vecB(2) += weightCoef*boundaryDisparity;

					++dataPointTotal;
				}
			} else {
				Segment neighborSegment = segmentConfiguration.segment(neighborSegmentIndex);
				int neighborSegmentPixelTotal = neighborSegment.segmentPixelTotal();
				double weightCoef = weightCoplanar/(segmentPixelTotal + neighborSegmentPixelTotal)*stepSize_*stepSize_;

				for (int pixelIndex = 0; pixelIndex < segmentPixelTotal; ++pixelIndex) {
					int pixelX = segment.segmentPixelX(pixelIndex);
					int pixelY = segment.segmentPixelY(pixelIndex);
					double pixelDisparity =  disparityPlaneParameters[neighborSegmentIndex][0]*pixelX
						+ disparityPlaneParameters[neighborSegmentIndex][1]*pixelY
						+ disparityPlaneParameters[neighborSegmentIndex][2];

					matA(0, 0) += weightCoef*pixelX*pixelX;
					matA(0, 1) += weightCoef*pixelX*pixelY;
					matA(0, 2) += weightCoef*pixelX;
					matA(1, 0) += weightCoef*pixelX*pixelY;
					matA(1, 1) += weightCoef*pixelY*pixelY;
					matA(1, 2) += weightCoef*pixelY;
					matA(2, 0) += weightCoef*pixelX;
					matA(2, 1) += weightCoef*pixelY;
					matA(2, 2) += weightCoef*1.0;
					vecB(0) += weightCoef*pixelDisparity*pixelX;
					vecB(1) += weightCoef*pixelDisparity*pixelY;
					vecB(2) += weightCoef*pixelDisparity;

					++dataPointTotal;
				}
				for (int neighborPixelIndex = 0; neighborPixelIndex < neighborSegmentPixelTotal; ++neighborPixelIndex) {
					int pixelX = neighborSegment.segmentPixelX(neighborPixelIndex);
					int pixelY = neighborSegment.segmentPixelY(neighborPixelIndex);
					double pixelDisparity =  disparityPlaneParameters[neighborSegmentIndex][0]*pixelX
						+ disparityPlaneParameters[neighborSegmentIndex][1]*pixelY
						+ disparityPlaneParameters[neighborSegmentIndex][2];

					matA(0, 0) += weightCoef*pixelX*pixelX;
					matA(0, 1) += weightCoef*pixelX*pixelY;
					matA(0, 2) += weightCoef*pixelX;
					matA(1, 0) += weightCoef*pixelX*pixelY;
					matA(1, 1) += weightCoef*pixelY*pixelY;
					matA(1, 2) += weightCoef*pixelY;
					matA(2, 0) += weightCoef*pixelX;
					matA(2, 1) += weightCoef*pixelY;
					matA(2, 2) += weightCoef*1.0;
					vecB(0) += weightCoef*pixelDisparity*pixelX;
					vecB(1) += weightCoef*pixelDisparity*pixelY;
					vecB(2) += weightCoef*pixelDisparity;

					++dataPointTotal;
				}
			}
		}

		if (dataPointTotal >= 3) {
			Eigen::Vector3d vecPlaneParameter = matA.colPivHouseholderQr().solve(vecB);
			disparityPlaneParameters[segmentIndex][0] = vecPlaneParameter(0);
			disparityPlaneParameters[segmentIndex][1] = vecPlaneParameter(1);
			disparityPlaneParameters[segmentIndex][2] = vecPlaneParameter(2);
		}

		double updateSquare = (disparityPlaneParameters[segmentIndex][0] - previousParameters[0])*(disparityPlaneParameters[segmentIndex][0] - previousParameters[0])
			+ (disparityPlaneParameters[segmentIndex][1] - previousParameters[1])*(disparityPlaneParameters[segmentIndex][1] - previousParameters[1])
			+ (disparityPlaneParameters[segmentIndex][2] - previousParameters[2])*(disparityPlaneParameters[segmentIndex][2] - previousParameters[2]);

		if (updateSquare > updateThresholdSquare) {
			newUpdateFlags[segmentIndex] = true;
			for (int ni = 0; ni < segment.neighborTotal(); ++ni) {
				newUpdateFlags[segment.neighborIndex(ni)] = true;
			}
		}
	}

	updateFlags = newUpdateFlags;
}

void Smoothfit::estimateSmoothFittingParameter(const SegmentConfiguration& segmentConfiguration,
												   const int segmentIndex,
												   const std::vector<int>& boundaryVariables,
												   const rev::Image<unsigned char>& outlierImage,
												   const std::vector< std::vector<double> >& disparityPlaneParameters,
												   std::vector<double>& smoothfittedParameters) const
{
	const double weightCoplanar = 0.2;
	const double weightHinge = 0.2;

	Eigen::Matrix3d matA;
	matA.setZero();
	Eigen::Vector3d vecB;
	vecB.setZero();

	Segment segment = segmentConfiguration.segment(segmentIndex);
	int segmentPixelTotal = segment.segmentPixelTotal();
	int dataPointTotal = 0;
	double weightData = 1.0/segmentPixelTotal;
	for (int pixelIndex = 0; pixelIndex < segmentPixelTotal; ++pixelIndex) {
		int pixelX = segment.segmentPixelX(pixelIndex);
		int pixelY = segment.segmentPixelY(pixelIndex);
		if (firstLeftDisparityImage_(pixelX, pixelY) == 0) continue;
		if (outlierImage(pixelX, pixelY) > 0) continue;

		matA(0, 0) += weightData*pixelX*pixelX;
		matA(0, 1) += weightData*pixelX*pixelY;
		matA(0, 2) += weightData*pixelX;
		matA(1, 0) += weightData*pixelX*pixelY;
		matA(1, 1) += weightData*pixelY*pixelY;
		matA(1, 2) += weightData*pixelY;
		matA(2, 0) += weightData*pixelX;
		matA(2, 1) += weightData*pixelY;
		matA(2, 2) += weightData*1.0;
		vecB(0) += weightData*firstLeftDisparityImage_(pixelX, pixelY)*pixelX;
		vecB(1) += weightData*firstLeftDisparityImage_(pixelX, pixelY)*pixelY;
		vecB(2) += weightData*firstLeftDisparityImage_(pixelX, pixelY);

		++dataPointTotal;
	}

	for (int neighborIndex = 0; neighborIndex < segment.boundaryTotal(); ++neighborIndex) {
		int neighborBoundaryIndex = segment.boundaryIndex(neighborIndex);
		if (boundaryVariables[neighborBoundaryIndex] < 2) continue;

		int neighborSegmentIndex = segmentConfiguration.boundary(neighborBoundaryIndex).segmentIndex(0);
		if (neighborSegmentIndex == segmentIndex) neighborSegmentIndex = segmentConfiguration.boundary(neighborBoundaryIndex).segmentIndex(1);

		if (boundaryVariables[neighborBoundaryIndex] == 2) {
			int boundaryPointTotal = segmentConfiguration.boundary(neighborBoundaryIndex).boundaryPixelTotal();
			double weightCoef = weightHinge/boundaryPointTotal;
			for (int boundaryPointIndex = 0; boundaryPointIndex < boundaryPointTotal; ++boundaryPointIndex) {
				double boundaryPointX = segmentConfiguration.boundary(neighborBoundaryIndex).boundaryPixelX(boundaryPointIndex);
				double boundaryPointY = segmentConfiguration.boundary(neighborBoundaryIndex).boundaryPixelY(boundaryPointIndex);
				double boundaryDisparity = disparityPlaneParameters[neighborSegmentIndex][0]*boundaryPointX
					+ disparityPlaneParameters[neighborSegmentIndex][1]*boundaryPointY
					+ disparityPlaneParameters[neighborSegmentIndex][2];

				matA(0, 0) += weightCoef*boundaryPointX*boundaryPointX;
				matA(0, 1) += weightCoef*boundaryPointX*boundaryPointY;
				matA(0, 2) += weightCoef*boundaryPointX;
				matA(1, 0) += weightCoef*boundaryPointX*boundaryPointY;
				matA(1, 1) += weightCoef*boundaryPointY*boundaryPointY;
				matA(1, 2) += weightCoef*boundaryPointY;
				matA(2, 0) += weightCoef*boundaryPointX;
				matA(2, 1) += weightCoef*boundaryPointY;
				matA(2, 2) += weightCoef*1.0;
				vecB(0) += weightCoef*boundaryDisparity*boundaryPointX;
				vecB(1) += weightCoef*boundaryDisparity*boundaryPointY;
				vecB(2) += weightCoef*boundaryDisparity;

				++dataPointTotal;
			}
		} else {
			Segment neighborSegment = segmentConfiguration.segment(neighborSegmentIndex);
			int neighborSegmentPixelTotal = neighborSegment.segmentPixelTotal();
			double weightCoef = weightCoplanar/(segmentPixelTotal + neighborSegmentPixelTotal);

			for (int pixelIndex = 0; pixelIndex < segmentPixelTotal; ++pixelIndex) {
				int pixelX = segment.segmentPixelX(pixelIndex);
				int pixelY = segment.segmentPixelY(pixelIndex);
				double pixelDisparity =  disparityPlaneParameters[neighborSegmentIndex][0]*pixelX
					+ disparityPlaneParameters[neighborSegmentIndex][1]*pixelY
					+ disparityPlaneParameters[neighborSegmentIndex][2];

				matA(0, 0) += weightCoef*pixelX*pixelX;
				matA(0, 1) += weightCoef*pixelX*pixelY;
				matA(0, 2) += weightCoef*pixelX;
				matA(1, 0) += weightCoef*pixelX*pixelY;
				matA(1, 1) += weightCoef*pixelY*pixelY;
				matA(1, 2) += weightCoef*pixelY;
				matA(2, 0) += weightCoef*pixelX;
				matA(2, 1) += weightCoef*pixelY;
				matA(2, 2) += weightCoef*1.0;
				vecB(0) += weightCoef*pixelDisparity*pixelX;
				vecB(1) += weightCoef*pixelDisparity*pixelY;
				vecB(2) += weightCoef*pixelDisparity;

				++dataPointTotal;
			}
			for (int neighborPixelIndex = 0; neighborPixelIndex < neighborSegmentPixelTotal; ++neighborPixelIndex) {
				int pixelX = neighborSegment.segmentPixelX(neighborPixelIndex);
				int pixelY = neighborSegment.segmentPixelY(neighborPixelIndex);
				double pixelDisparity =  disparityPlaneParameters[neighborSegmentIndex][0]*pixelX
					+ disparityPlaneParameters[neighborSegmentIndex][1]*pixelY
					+ disparityPlaneParameters[neighborSegmentIndex][2];

				matA(0, 0) += weightCoef*pixelX*pixelX;
				matA(0, 1) += weightCoef*pixelX*pixelY;
				matA(0, 2) += weightCoef*pixelX;
				matA(1, 0) += weightCoef*pixelX*pixelY;
				matA(1, 1) += weightCoef*pixelY*pixelY;
				matA(1, 2) += weightCoef*pixelY;
				matA(2, 0) += weightCoef*pixelX;
				matA(2, 1) += weightCoef*pixelY;
				matA(2, 2) += weightCoef*1.0;
				vecB(0) += weightCoef*pixelDisparity*pixelX;
				vecB(1) += weightCoef*pixelDisparity*pixelY;
				vecB(2) += weightCoef*pixelDisparity;

				++dataPointTotal;
			}
		}
	}

	smoothfittedParameters = disparityPlaneParameters[segmentIndex];
	if (dataPointTotal >= 3) {
		Eigen::Vector3d vecPlaneParameter = matA.colPivHouseholderQr().solve(vecB);
		smoothfittedParameters[0] = vecPlaneParameter(0);
		smoothfittedParameters[1] = vecPlaneParameter(1);
		smoothfittedParameters[2] = vecPlaneParameter(2);
	}
}


void Smoothfit::interpolatePlaneParameters(const SegmentConfiguration& segmentConfiguration, std::vector< std::vector<double> >& disparityPlaneParameters) const {
	rev::Image<float> interpolatedDisparityImage = interpolateDisparityImage(firstLeftDisparityImage_);

	const double confidenceLevel = 0.99;

	int segmentTotal = segmentConfiguration.segmentTotal();
	disparityPlaneParameters.resize(segmentTotal);

	for (int segmentIndex = 0; segmentIndex < segmentTotal; ++segmentIndex) {
		if (disparityPlaneParameters[segmentIndex][0] != 0.0
			|| disparityPlaneParameters[segmentIndex][1] != 0.0
			|| disparityPlaneParameters[segmentIndex][2] != 0.0) continue;

		Segment segment = segmentConfiguration.segment(segmentIndex);

		if (segment.segmentPixelTotal() < 3) {
			double dispSum = 0;
			int dispPixTotal = 0;
			for (int pixelIndex = 0; pixelIndex < segment.segmentPixelTotal(); ++pixelIndex) {
				int pixelX = segment.segmentPixelX(pixelIndex);
				int pixelY = segment.segmentPixelY(pixelIndex);
				if (interpolatedDisparityImage(pixelX, pixelY) == 0) continue;

				++dispPixTotal;
				dispSum += interpolatedDisparityImage(pixelX, pixelY);
			}

			if (dispPixTotal == 0) {
				disparityPlaneParameters[segmentIndex][0] = 0;
				disparityPlaneParameters[segmentIndex][1] = 0;
				disparityPlaneParameters[segmentIndex][2] = 0;
			} else {
				double aveDisp = dispSum/dispPixTotal;
				disparityPlaneParameters[segmentIndex][0] = 0;
				disparityPlaneParameters[segmentIndex][1] = 0;
				disparityPlaneParameters[segmentIndex][2] = aveDisp;
			}
			continue;
		}

		std::vector<DisparityPixelS> disparityPixels;
		for (int pixelIndex = 0; pixelIndex < segment.segmentPixelTotal(); ++pixelIndex) {
			int pixelX = segment.segmentPixelX(pixelIndex);
			int pixelY = segment.segmentPixelY(pixelIndex);

			DisparityPixelS newDisparityPixel;
			newDisparityPixel.normalizedX = pixelX;// - segment.centerX();
			newDisparityPixel.normalizedY = pixelY;// - segment.centerY();
			newDisparityPixel.disparity = interpolatedDisparityImage(pixelX, pixelY);
			disparityPixels.push_back(newDisparityPixel);
		}
		int disparityPixelTotal = static_cast<int>(disparityPixels.size());

		disparityPlaneParameters[segmentIndex].resize(3);
		int bestInlierTotal = 0;
		std::vector<bool> bestInlierFlags(disparityPixelTotal);
		int samplingTotal = disparityPixelTotal;
		int samplingCount = 0;
		while (samplingCount < samplingTotal) {
			int drawIndices[3];
			drawIndices[0] = rand()%disparityPixelTotal;
			drawIndices[1] = rand()%disparityPixelTotal;
			while (drawIndices[1] == drawIndices[0]) drawIndices[1] = rand()%disparityPixelTotal;
			drawIndices[2] = rand()%disparityPixelTotal;
			while (drawIndices[2] == drawIndices[0] || drawIndices[2] == drawIndices[1]) drawIndices[2] = rand()%disparityPixelTotal;

			Eigen::Matrix3d matPosition;
			Eigen::Vector3d vecDisparity;
			for (int i = 0; i < 3; i++) {
				matPosition(i, 0) = disparityPixels[drawIndices[i]].normalizedX;
				matPosition(i, 1) = disparityPixels[drawIndices[i]].normalizedY;
				matPosition(i, 2) = 1.0;
				vecDisparity(i) = disparityPixels[drawIndices[i]].disparity;
			}
			Eigen::Vector3d param = matPosition.colPivHouseholderQr().solve(vecDisparity);

			// Count the number of inliers
			int inlierTotal = 0;
			std::vector<bool> inlierFlags(disparityPixelTotal);
			for (int i = 0; i < disparityPixelTotal; ++i) {
				double estimateDisparity = param(0)*disparityPixels[i].normalizedX + param(1)*disparityPixels[i].normalizedY + param(2);
				if (fabs(estimateDisparity - disparityPixels[i].disparity) <= 2.0) {
					++inlierTotal;
					inlierFlags[i] = true;
				} else {
					inlierFlags[i] = false;
				}
			}

			if (inlierTotal > bestInlierTotal) {
				bestInlierTotal = inlierTotal;
				bestInlierFlags = inlierFlags;

				samplingTotal = computeRequiredSamplingTotal(3, bestInlierTotal, disparityPixelTotal, samplingTotal, confidenceLevel);
			}

			++samplingCount;
		}

		Eigen::MatrixXd matPosition(bestInlierTotal, 3);
		Eigen::VectorXd vecDisparity(bestInlierTotal);
		int inlierIndex = 0;
		for (int i = 0; i < disparityPixelTotal; ++i) {
			if (bestInlierFlags[i]) {
				matPosition(inlierIndex, 0) = disparityPixels[i].normalizedX;
				matPosition(inlierIndex, 1) = disparityPixels[i].normalizedY;
				matPosition(inlierIndex, 2) = 1.0;
				vecDisparity(inlierIndex) = disparityPixels[i].disparity;
				++inlierIndex;
			}
		}
		Eigen::Vector3d vecPlaneParameter = matPosition.colPivHouseholderQr().solve(vecDisparity);
		disparityPlaneParameters[segmentIndex][0] = vecPlaneParameter(0);
		disparityPlaneParameters[segmentIndex][1] = vecPlaneParameter(1);
		disparityPlaneParameters[segmentIndex][2] = vecPlaneParameter(2);
	}
}

void Smoothfit::makeBoundaryLabelImage(const SegmentConfiguration& segmentConfiguration, const std::vector<int>& boundaryLabels) {
    const rev::Image<unsigned short>& segmentIndexImage = segmentConfiguration.segmentIndexImage();
    int width = segmentIndexImage.width();
    int height = segmentIndexImage.height();

    boundaryLabelImage_.resize(width, height, 3);
    boundaryLabelImage_.setTo(rev::Color<unsigned char>(255,255,255));

	int coplanarWidth = 2;
    for (int boundaryIndex = 0; boundaryIndex < segmentConfiguration.boundaryTotal(); ++boundaryIndex) {
		if (boundaryLabels[boundaryIndex] != 3) continue;

        const SegmentBoundary& boundary = segmentConfiguration.boundary(boundaryIndex);
        for (int pixelIndex = 0; pixelIndex < boundary.boundaryPixelTotal(); ++pixelIndex) {
            double pixelX = boundary.boundaryPixelX(pixelIndex);
            double pixelY = boundary.boundaryPixelY(pixelIndex);

            int px = static_cast<int>(pixelX);
            int py = static_cast<int>(pixelY);

            if (pixelX - px > 0) {
				for (int w = 0; w < coplanarWidth - 1; ++w) {
					if (px - w >= 0) boundaryLabelImage_.setColor(px - w, py, rev::Color<unsigned char>(128, 128, 128));
				}
				for (int w = 1; w < coplanarWidth; ++w) {
					if (px + w < width) boundaryLabelImage_.setColor(px + w, py, rev::Color<unsigned char>(128, 128, 128));
				}
			} else {
				for (int w = 0; w < coplanarWidth - 1; ++w) {
					if (py - w >= 0) boundaryLabelImage_.setColor(px, py - w, rev::Color<unsigned char>(128, 128, 128));
				}
				for (int w = 1; w < coplanarWidth; ++w) {
					if (py + w < height) boundaryLabelImage_.setColor(px, py + w, rev::Color<unsigned char>(128, 128, 128));
				}
			}
		}
	}

	int boundaryWidth = 7;
    for (int boundaryIndex = 0; boundaryIndex < segmentConfiguration.boundaryTotal(); ++boundaryIndex) {
		if (boundaryLabels[boundaryIndex] == 3) continue;

		const SegmentBoundary& boundary = segmentConfiguration.boundary(boundaryIndex);
        for (int pixelIndex = 0; pixelIndex < boundary.boundaryPixelTotal(); ++pixelIndex) {
            double pixelX = boundary.boundaryPixelX(pixelIndex);
            double pixelY = boundary.boundaryPixelY(pixelIndex);

            int px = static_cast<int>(pixelX);
            int py = static_cast<int>(pixelY);

			rev::Color<unsigned char> negativeSideColor, positiveSideColor;
			if (boundaryLabels[boundaryIndex] == 2) {
				negativeSideColor.set(0, 225, 0);
				positiveSideColor.set(0, 225, 0);
			} else if (segmentIndexImage(px, py) == boundary.segmentIndex(boundaryLabels[boundaryIndex])) {
				negativeSideColor.set(225, 0, 0);
				positiveSideColor.set(0, 0, 225);
			} else {
				negativeSideColor.set(0, 0, 225);
				positiveSideColor.set(225, 0, 0);
			}

            if (pixelX - px > 0) {
				for (int w = 0; w < boundaryWidth - 1; ++w) {
					if (px - w >= 0) boundaryLabelImage_.setColor(px - w, py, negativeSideColor);
				}
				for (int w = 1; w < boundaryWidth; ++w) {
					if (px + w < width) boundaryLabelImage_.setColor(px + w, py, positiveSideColor);
				}
			} else {
				for (int w = 0; w < boundaryWidth - 1; ++w) {
					if (py - w >= 0) boundaryLabelImage_.setColor(px, py - w, negativeSideColor);
				}
				for (int w = 1; w < boundaryWidth; ++w) {
					if (py + w < height) boundaryLabelImage_.setColor(px, py + w, positiveSideColor);
				}
			}
        }
    }

}

void Smoothfit::makeBoundaryLabelList(const SegmentConfiguration& segmentConfiguration, const std::vector<int>& boundaryLabels) {
	int boundaryTotal = segmentConfiguration.boundaryTotal();
	boundaryLabelList_.resize(boundaryTotal);
	for (int i = 0; i < boundaryTotal; ++i) {
		boundaryLabelList_[i].resize(3);
		boundaryLabelList_[i][0] = segmentConfiguration.boundary(i).segmentIndex(0);
		boundaryLabelList_[i][1] = segmentConfiguration.boundary(i).segmentIndex(1);
		boundaryLabelList_[i][2] = boundaryLabels[i];
	}
}

int computeRequiredSamplingTotal(const int drawTotal, const int inlierTotal, const int pointTotal, const int currentSamplingTotal, const double confidenceLevel) {
	double ep = 1 - static_cast<double>(inlierTotal)/static_cast<double>(pointTotal);
	if (ep == 1.0) {
		ep = 0.5;
	}

	int newSamplingTotal = static_cast<int>(log(1 - confidenceLevel)/log(1 - pow(1 - ep, drawTotal)) + 0.5);
	if (newSamplingTotal < currentSamplingTotal) {
		return newSamplingTotal;
	} else {
		return currentSamplingTotal;
	}
}


void Smoothfit::estimateOutlierImage(const SegmentConfiguration& segmentConfiguration,
										 std::vector< std::vector<double> >& disparityPlaneParameters,
										 rev::Image<unsigned char>& outlierImage)
{
	int segmentTotal = segmentConfiguration.segmentTotal();
	disparityPlaneParameters.resize(segmentTotal);
	for (int i = 0; i < segmentTotal; ++i) {
		disparityPlaneParameters[i].resize(3);
		disparityPlaneParameters[i][0] = seeds_[i].disparityPlane[0];
		disparityPlaneParameters[i][1] = seeds_[i].disparityPlane[1];
		disparityPlaneParameters[i][2] = seeds_[i].disparityPlane[2];
	}
}
