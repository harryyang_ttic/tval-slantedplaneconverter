#include "interpolateDisparityImage.h"

rev::Image<unsigned short> interpolateDisparityImage(const rev::Image<unsigned short>& disparityImage) {
	int width = disparityImage.width();
	int height = disparityImage.height();

	rev::Image<unsigned short> interpolatedImage = disparityImage;

	for (int y = 0; y < height; ++y) {
		// initialize counter
		int count = 0;

		for (int x = 0; x < width; ++x) {
			if (interpolatedImage(x, y) > 0) {   // if disparity is valid
				if (count >= 1) {   // at least one pixel requires interpolation
					// first and last value for interpolation
					int startX = x - count;
					int endX = x - 1;

					// set pixel to min disparity
					if (startX > 0 && endX < width-1) {
						unsigned short interpolationDisparity = std::min(interpolatedImage(startX-1, y), interpolatedImage(endX+1, y));
						for (int interpolateX = startX; interpolateX <= endX; ++interpolateX) {
							interpolatedImage(interpolateX, y) = interpolationDisparity;
						}
					}
				}

				// reset counter
				count = 0;
			} else {   // otherwise increment counter
				++count;
			}
		}

		// extrapolate to the left
		for (int x = 0; x < width; ++x) {
			if (interpolatedImage(x, y) > 0) {
				for (int interpolateX = 0; interpolateX < x; ++interpolateX) {
					interpolatedImage(interpolateX, y) = interpolatedImage(x, y);
				}
				break;
			}
		}

		// extrapolate to the right
		for (int x = width-1; x >= 0; --x) {
			if (interpolatedImage(x, y) > 0) {
				for (int interpolateX = x+1; interpolateX < width; ++interpolateX) {
					interpolatedImage(interpolateX, y) = interpolatedImage(x, y);
				}
				break;
			}
		}
	}

	// for each column
	for (int x = 0; x < width; ++x) {
		// extrapolate to the top
		for (int y = 0; y < height; ++y) {
			if (interpolatedImage(x, y) > 0) {
				for (int interpolateY = 0; interpolateY < y; ++interpolateY) {
					interpolatedImage(x, interpolateY) = interpolatedImage(x, y);
				}
				break;
			}
		}

		// extrapolate to the bottom
		for (int y = height-1; y >= 0; --y) {
			if (interpolatedImage(x, y) > 0) {
				for (int interpolateY = y+1; interpolateY < height; ++interpolateY) {
					interpolatedImage(x, interpolateY) = interpolatedImage(x, y);
				}
				break;
			}
		}
	}

	return interpolatedImage;
}

rev::Image<double> interpolateDisparityImage(const rev::Image<double>& disparityImage) {
	int width = disparityImage.width();
	int height = disparityImage.height();

	rev::Image<double> interpolatedImage = disparityImage;

	for (int y = 0; y < height; ++y) {
		// initialize counter
		int count = 0;

		for (int x = 0; x < width; ++x) {
			if (interpolatedImage(x, y) > 0) {   // if disparity is valid
				if (count >= 1) {   // at least one pixel requires interpolation
					// first and last value for interpolation
					int startX = x - count;
					int endX = x - 1;

					// set pixel to min disparity
					if (startX > 0 && endX < width-1) {
						double interpolationDisparity = std::min(interpolatedImage(startX-1, y), interpolatedImage(endX+1, y));
						for (int interpolateX = startX; interpolateX <= endX; ++interpolateX) {
							interpolatedImage(interpolateX, y) = interpolationDisparity;
						}
					}
				}

				// reset counter
				count = 0;
			} else {   // otherwise increment counter
				++count;
			}
		}

		// extrapolate to the left
		for (int x = 0; x < width; ++x) {
			if (interpolatedImage(x, y) > 0) {
				for (int interpolateX = 0; interpolateX < x; ++interpolateX) {
					interpolatedImage(interpolateX, y) = interpolatedImage(x, y);
				}
				break;
			}
		}

		// extrapolate to the right
		for (int x = width-1; x >= 0; --x) {
			if (interpolatedImage(x, y) > 0) {
				for (int interpolateX = x+1; interpolateX < width; ++interpolateX) {
					interpolatedImage(interpolateX, y) = interpolatedImage(x, y);
				}
				break;
			}
		}
	}

	// for each column
	for (int x = 0; x < width; ++x) {
		// extrapolate to the top
		for (int y = 0; y < height; ++y) {
			if (interpolatedImage(x, y) > 0) {
				for (int interpolateY = 0; interpolateY < y; ++interpolateY) {
					interpolatedImage(x, interpolateY) = interpolatedImage(x, y);
				}
				break;
			}
		}

		// extrapolate to the bottom
		for (int y = height-1; y >= 0; --y) {
			if (interpolatedImage(x, y) > 0) {
				for (int interpolateY = y+1; interpolateY < height; ++interpolateY) {
					interpolatedImage(x, interpolateY) = interpolatedImage(x, y);
				}
				break;
			}
		}
	}

	return interpolatedImage;
}

rev::Image<float> interpolateDisparityImage(const rev::Image<float>& disparityImage) {
	int width = disparityImage.width();
	int height = disparityImage.height();

	rev::Image<float> interpolatedImage = disparityImage;

	for (int y = 0; y < height; ++y) {
		// initialize counter
		int count = 0;

		for (int x = 0; x < width; ++x) {
			if (interpolatedImage(x, y) > 0) {   // if disparity is valid
				if (count >= 1) {   // at least one pixel requires interpolation
					// first and last value for interpolation
					int startX = x - count;
					int endX = x - 1;

					// set pixel to min disparity
					if (startX > 0 && endX < width-1) {
						float interpolationDisparity = std::min(interpolatedImage(startX-1, y), interpolatedImage(endX+1, y));
						for (int interpolateX = startX; interpolateX <= endX; ++interpolateX) {
							interpolatedImage(interpolateX, y) = interpolationDisparity;
						}
					}
				}

				// reset counter
				count = 0;
			} else {   // otherwise increment counter
				++count;
			}
		}

		// extrapolate to the left
		for (int x = 0; x < width; ++x) {
			if (interpolatedImage(x, y) > 0) {
				for (int interpolateX = 0; interpolateX < x; ++interpolateX) {
					interpolatedImage(interpolateX, y) = interpolatedImage(x, y);
				}
				break;
			}
		}

		// extrapolate to the right
		for (int x = width-1; x >= 0; --x) {
			if (interpolatedImage(x, y) > 0) {
				for (int interpolateX = x+1; interpolateX < width; ++interpolateX) {
					interpolatedImage(interpolateX, y) = interpolatedImage(x, y);
				}
				break;
			}
		}
	}

	// for each column
	for (int x = 0; x < width; ++x) {
		// extrapolate to the top
		for (int y = 0; y < height; ++y) {
			if (interpolatedImage(x, y) > 0) {
				for (int interpolateY = 0; interpolateY < y; ++interpolateY) {
					interpolatedImage(x, interpolateY) = interpolatedImage(x, y);
				}
				break;
			}
		}

		// extrapolate to the bottom
		for (int y = height-1; y >= 0; --y) {
			if (interpolatedImage(x, y) > 0) {
				for (int interpolateY = y+1; interpolateY < height; ++interpolateY) {
					interpolatedImage(x, interpolateY) = interpolatedImage(x, y);
				}
				break;
			}
		}
	}

	return interpolatedImage;
}
