#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <time.h>
#include <cmdline.h>
#include <revlib.h>
#include "Smoothfit.h"

struct ParameterStereoSlic {
	bool verbose;
	double disparityFactor;
	int superpixelTotal;
	std::string firstLeftImageFilename;
	std::string firstLeftDisparityImageFilename;

	std::string outputSegmentImageFilename;
	std::string outputDisparityPlaneFilename;
	double fx;
	double fy;
	double ox;
	double oy;
	double B;
};

// Prototype declaration
ParameterStereoSlic parseCommandline(int argc, char* argv[]);

ParameterStereoSlic parseCommandline(int argc, char* argv[]) {
	// Make command parser
	cmdline::parser commandParser;
	commandParser.add<double>("factor", 'f', "disparity factor of input disparity image", false, 256.0);
	commandParser.add<int>("superpixel", 's', "the number of superpixels", false, 2000);
	commandParser.add<double>("fx",'x',"focal length x",false,999.7838133341337);
	commandParser.add<double>("fy",'y',"focal length y",false,999.7838133341337);
	commandParser.add<double>("ox",'w',"focal origin x",false,792.9412307739258);
	commandParser.add<double>("oy",'z',"focal origin y",false,609.6846313476562);
	commandParser.add<double>("baseline",'b',"baseline b",false,0.53084992269);
	commandParser.add("verbose", 'v', "verbose");
	commandParser.add("help", 'h', "display this message");
	commandParser.set_program_name("SlantedPlaneConverter");
	commandParser.footer("imgL0 dispL0 segFolder segPlaneFolder");

	// Parse command line
	bool isCorrectCommandline = commandParser.parse(argc, argv);
	// Check arguments
	if (!isCorrectCommandline) {
		std::cerr << commandParser.error() << std::endl;
	}
	if (!isCorrectCommandline || commandParser.exist("help") || commandParser.rest().size() < 2) {
		std::cerr << commandParser.usage() << std::endl;
		exit(1);
	}

	// Set program parameters
	ParameterStereoSlic parameters;
	// Verbose flag
	parameters.verbose = commandParser.exist("verbose");
	// Disparity factor
	parameters.disparityFactor = commandParser.get<double>("factor");
	// The number of superpixels
	parameters.superpixelTotal = commandParser.get<int>("superpixel");
	parameters.fx=commandParser.get<double>("fx");
	parameters.fy=commandParser.get<double>("fy");
	parameters.ox=commandParser.get<double>("ox");
	parameters.oy=commandParser.get<double>("oy");
	parameters.B=commandParser.get<double>("baseline");

	// Input files
	parameters.firstLeftImageFilename = commandParser.rest()[0];
	parameters.firstLeftDisparityImageFilename = commandParser.rest()[1];

	std::string segFolder = commandParser.rest()[2];
	std::string segPlaneFolder = commandParser.rest()[3];
	//parameters.cameraMotionFilename = commandParser.rest()[2];
	// Output files
	//Harry update 01-28, so that the output files are in the destination directory
    std::string outputSegmentImageFilename = parameters.firstLeftImageFilename;
    unsigned slash = outputSegmentImageFilename.find_last_of("/");
    outputSegmentImageFilename = outputSegmentImageFilename.substr(slash+1);
    size_t dotPosition = outputSegmentImageFilename.find('.');
    if (dotPosition != std::string::npos) outputSegmentImageFilename.erase(dotPosition);

    parameters.outputSegmentImageFilename = segFolder + "/" + outputSegmentImageFilename;
    parameters.outputDisparityPlaneFilename = segPlaneFolder + "/" + outputSegmentImageFilename;

    parameters.outputSegmentImageFilename += "_seg.png";
    parameters.outputDisparityPlaneFilename += "_seg_planes.txt";

	return parameters;
}

void writeBoundaryLabelFile(const std::string boundaryLabelFilename, const std::vector< std::vector<int> >& boundaryLabelList) {
	std::ofstream outputFileStream(boundaryLabelFilename.c_str(), std::ios_base::out);
	if (outputFileStream.fail()) {
		std::cerr << "error: can't open file (" << boundaryLabelFilename << ")" << std::endl;
		exit(1);
	}

	for (int i = 0; i < boundaryLabelList.size(); ++i) {
		outputFileStream << boundaryLabelList[i][0] << " ";
		outputFileStream << boundaryLabelList[i][1] << " ";
		outputFileStream << boundaryLabelList[i][2] << std::endl;
	}

	outputFileStream.close();
}

//by harry
void writeDisparityPlaneFile(const std::string disparityPlaneFileName, const std::vector< std::vector<double> >& disparityPlanes,
double fx, double fy, double ox, double oy, double B){
    std::ofstream outputFileStream(disparityPlaneFileName.c_str(), std::ios_base::out);
    if(outputFileStream.fail()){
		std::cerr << "error: can't open file (" << disparityPlaneFileName << ")" << std::endl;
		exit(1);
	}

    outputFileStream<<"camera intrinsic parameters (fx, fy, ox, oy, baseline): "<<fx<<" "<<fy<<" "<<ox<<" "<<oy<<" "<<B<<std::endl;
	for(int i=0;i<disparityPlanes.size();i++){
        outputFileStream << i <<" ";
        double a=disparityPlanes[i][0], b=disparityPlanes[i][1], c=disparityPlanes[i][2];
        outputFileStream << a << " " <<b<<" "<<"-1"<<" "<<c<<" ";
        double plane_a=fx*a/(fx*B),
        plane_b=fy*b/(fx*B),
        plane_c=(a*ox+b*oy+c)/(fx*B);
        outputFileStream << plane_a << " " << plane_b <<" "<<plane_c<<" "<<"-1"<<" ";
        double outlier=disparityPlanes[i][3];
        if(outlier==0)
            outputFileStream<<"N"<<std::endl;
        else
            outputFileStream<<"Y"<<std::endl;
	}
	outputFileStream.close();
}

std::string getFilenameExtension(const std::string filename) {
    std::string filenameExtension = filename;
    size_t dotPosition = filenameExtension.rfind('.');
    if (dotPosition != std::string::npos) {
        filenameExtension.erase(0, dotPosition+1);
    } else {
        filenameExtension = "";
    }

    return filenameExtension;
}


int main(int argc, char* argv[]) {
	// Parse command line
	ParameterStereoSlic parameters = parseCommandline(argc, argv);

	try {
		// Open input image
		rev::Image<unsigned char> firstLeftImage = rev::readImageFile(parameters.firstLeftImageFilename);
		//by Harry
		std::string extension=getFilenameExtension(parameters.firstLeftDisparityImageFilename);
		rev::Image<unsigned short> firstLeftDisparityImage;
		if(extension == "pgm")
		{
            firstLeftDisparityImage=rev::readUShortPGM(parameters.firstLeftDisparityImageFilename);
		}
		else
		{
            firstLeftDisparityImage=rev::read16bitImageFile(parameters.firstLeftDisparityImageFilename);
		}
		//CameraMotion cameraMotion;
		//cameraMotion.readCameraMotionFile(parameters.cameraMotionFilename);

		clock_t startClock, endClock;
		startClock = clock();

		// StereoSLIC
		Smoothfit smoothfit;
		// Perform superpixel segmentation
		rev::Image<unsigned short> segmentImage;
		rev::Image<unsigned short> segmentDisparityImage;
		rev::Image<unsigned short> segmentFlowImage;
		rev::Image<unsigned char> boundaryLabelImage;
		std::vector< std::vector<int> > boundaryLabelList;
        // by harry, segmented plane list
        std::vector< std::vector<double> > disparityPlanes;

		/*smoothfit.segment(parameters.superpixelTotal,
			firstLeftImage, firstLeftDisparityImage,
			cameraMotion, 256,
			segmentImage, segmentDisparityImage, segmentFlowImage,
			boundaryLabelImage, boundaryLabelList,
			disparityPlanes);*/
        smoothfit.segment(parameters.superpixelTotal,
			firstLeftImage, firstLeftDisparityImage, 256,
			segmentImage, segmentDisparityImage,
			boundaryLabelImage, boundaryLabelList,
			disparityPlanes);

		endClock = clock();
		if (parameters.verbose) {
			std::cerr << "Computation time: " << static_cast<double>(endClock - startClock)/CLOCKS_PER_SEC << " sec." << std::endl;
			std::cerr << std::endl;
		}

		// Output
		rev::write16bitImageFile(parameters.outputSegmentImageFilename, segmentImage);
		writeDisparityPlaneFile(parameters.outputDisparityPlaneFilename,disparityPlanes,
		parameters.fx, parameters.fy, parameters.ox, parameters.oy, parameters.B);

	} catch (const rev::Exception& revException) {
		std::cerr << "Error [" << revException.functionName() << "]: " << revException.message() << std::endl;
		exit(1);
	}
}
