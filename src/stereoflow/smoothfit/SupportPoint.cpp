#include "SupportPoint.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include "StereoException.h"

void SupportPoint::clear() {
    x_ = -1;
    y_ = -1;
    disparities_.clear();
}

void SupportPoint::appendDisparity(const double disparity, const double error) {
    DisparityCandidate newDisparityCandidate;
    newDisparityCandidate.disparity = disparity;
    newDisparityCandidate.error = error;
    disparities_.push_back(newDisparityCandidate);
    
    sort(disparities_.begin(), disparities_.end());
}

double SupportPoint::disparity(const int candidateIndex) const {
    if (candidateIndex < 0 || candidateIndex >= static_cast<int>(disparities_.size())) {
        throw StereoException("SupportPoint::disparity", "index is not valid");
    }
    
    return disparities_[candidateIndex].disparity;
}

double SupportPoint::disparityError(const int candidateIndex) const {
    if (candidateIndex < 0 || candidateIndex >= static_cast<int>(disparities_.size())) {
        throw StereoException("SupportPoint::disparityError", "index is not valid");
    }
    
    return disparities_[candidateIndex].error;
}

bool SupportPoint::DisparityCandidate::operator<(const DisparityCandidate& comparisonDisparityCandidate) const {
    return (this->error < comparisonDisparityCandidate.error);
}

std::vector<SupportPoint> readSupportPointFile(const std::string& filename) {
	std::ifstream pointFileStream;
    pointFileStream.open(filename.c_str(), std::ios_base::in);
    if (!pointFileStream) {
		std::stringstream errorMessage;
        errorMessage << "can't open support point file (" << filename << ")";
        throw StereoException("readSupportPointFile", errorMessage.str());
    }
    
	std::vector<SupportPoint> supportPoints;
    
    while (pointFileStream.peek() != -1) {
        int x, y;
        int disparityTotal;
        pointFileStream >> x;
        pointFileStream >> y;
        SupportPoint newSupportPoint(x, y);
        
        pointFileStream >> disparityTotal;
        for (int i = 0; i < disparityTotal; ++i) {
            double disparity;
            double error;
            pointFileStream >> disparity;
            pointFileStream >> error;
            newSupportPoint.appendDisparity(disparity, error);
        }
        pointFileStream.get();
        
        supportPoints.push_back(newSupportPoint);
    }
    pointFileStream.close();
    
    return supportPoints;
}

void writeSupportPointFile(const std::string& filename, const std::vector<SupportPoint>& supportPoints) {
	std::ofstream pointFileStream;
    pointFileStream.open(filename.c_str(), std::ios_base::out);
    if (!pointFileStream) {
		std::stringstream errorMessage;
        errorMessage << "can't open support point file (" << filename << ")";
        throw StereoException("writeSupportPointFile", errorMessage.str());
    }
    
    int pointTotal = static_cast<int>(supportPoints.size());
    for (int pointIndex = 0; pointIndex < pointTotal; ++pointIndex) {
        pointFileStream << supportPoints[pointIndex].x() << " " << supportPoints[pointIndex].y() << " ";
        int disparityTotal = supportPoints[pointIndex].disparityTotal();
        pointFileStream << disparityTotal;
        for (int candidateIndex = 0; candidateIndex < disparityTotal; ++candidateIndex) {
            pointFileStream << " " << supportPoints[pointIndex].disparity(candidateIndex);
            pointFileStream << " " << supportPoints[pointIndex].disparityError(candidateIndex);
        }
        pointFileStream << std::endl;
    }
    pointFileStream.close();
    
}
