#pragma once

#include <vector>
#include <revlib.h>
#include "SegmentConfiguration.h"

class Smoothfit {
public:
	Smoothfit();

	void setIterationTotal(const int iterationTotal);
	void setWeightParameters(const double compactnessWeight,
		const double disparityWeight,
		const double noDisparityPenalty);

	void segment(const int superpixelTotal,
		const rev::Image<unsigned char>& firstLeftImage,
		const rev::Image<unsigned short>& firstLeftDisparityShortImage,
		const double disparityFactor,
		rev::Image<unsigned short>& segmentImage,
		rev::Image<unsigned short>& segmentDisparityImage,
		rev::Image<unsigned char>& boundaryLabelImage,
		std::vector< std::vector<int> >& boundaryLabelList,
		std::vector< std::vector<double> >& disparityPlanes);

private:
	struct LabXYD {
		LabXYD() {
			colorSum[0] = 0;  colorSum[1] = 0;  colorSum[2] = 0;
			color[0] = 0;  color[1] = 0;  color[2] = 0;
			positionSum[0] = 0;  positionSum[1] = 0;
			position[0] = 0;  position[1] = 0;
			disparityPlane[0] = 0;  disparityPlane[1] = 0;  disparityPlane[2] = -1;
		}

		int pixelTotal;
		double colorSum[3];
		double color[3];
		double positionSum[2];
		double position[2];
		double disparityPlane[3];
	};
	struct DisparityPixel {
		double x;
		double y;
		double d;
	};
	struct ParameterUpdate {
		int segmentIndex;
		double updateValue;

		bool operator<(const ParameterUpdate& comparisonUpdate) { return updateValue < comparisonUpdate.updateValue; }
	};

	void setColorAndDisparity(const rev::Image<unsigned char>& firstLeftImage,
		const rev::Image<unsigned short>& firstLeftDisparityShortImage,
		const double disparityFactor);
	void initializeSeeds(const int superpixelTotal);
	void performSegmentation();
	void makeDisparityImage(const double disparityFactor, rev::Image<unsigned short>& disparityImage);
    //by Harry
    void saveDisparityPlanes(const double disparityFactor, std::vector<std::vector< double> >& disparityPlanes);

	void checkInputImages(const rev::Image<unsigned char>& firstLeftImage,
		const rev::Image<unsigned short>& firstLeftDisparityShortImage) const;
	void setStereoLabImages(const rev::Image<unsigned char>& firstLeftImage);
	void calcLeftLabEdges();
	void setDisparityImages(const rev::Image<unsigned short>& firstLeftDisparityShortImage,
		const double disparityFactor);
	void setGridSeedPoint(const int superpixelTotal);
	void perturbSeeds();
	void initializeAssignment();
	void assignLabelWithConnectivity();
	bool isBoundaryPixel(const int x, const int y) const;
	bool isInfeasible(const int x, const int y) const;
	std::vector<int> getNeighborSeedIndices(const int x, const int y) const;
	double computePixelEnergy(const int x, const int y, const int seedIndex) const;
	double computeBoundaryLengthEnergy(const int x, const int y, const int seedIndex) const;
	void updateSeeds();
	void updateSeedsColorAndPosition();
	void estimateDisparityPlaneParameter();
	std::vector< std::vector<DisparityPixel> > makeDisparityPixelList() const;
	std::vector<double> estimateDisparityPlaneParameter(const std::vector<DisparityPixel>& disparityPixels) const;
	std::vector<double> estimateDisparityPlaneParameterRANSAC(const std::vector<DisparityPixel>& disparityPixels) const;
	void solvePlaneEquations(const double x1, const double y1, const double z1, const double d1,
		const double x2, const double y2, const double z2, const double d2,
		const double x3, const double y3, const double z3, const double d3,
		std::vector<double>& planeParameter) const;
	void labelConnectedPixels(const int x,
		const int y,
		const int newLabelIndex,
		std::vector<int>& newLabels,
		std::vector<int>& connectedXs,
		std::vector<int>& connectedYs) const;
	void subpixelLabImageIntensity(const rev::Image<float>& labImage,
		const double x, const double y,
		double& subpixelL, double& subpixelA, double& subpixelB) const;
	double subpixelDisparityImageIntensity(const rev::Image<float>& disparityImage,
		const double x, const double y) const;

	void estimateInitialFit(const SegmentConfiguration& segmentConfiguration,
		std::vector< std::vector<double> >& disparityPlaneParameters,
		rev::Image<unsigned char>& outlierImage) const;
	void estimateBoundaryParameters(const SegmentConfiguration& segmentConfiguration,
		const std::vector< std::vector<double> >& disparityPlaneParameters,
		std::vector<int>& boundaryVariables) const;
	void estimateSmoothFit(const SegmentConfiguration& segmentConfiguration,
		const std::vector<int>& boundaryVariables,
		const rev::Image<unsigned char>& outlierImage,
		std::vector< std::vector<double> >& disparityPlaneParameters,
		std::vector<bool>& updateFlags) const;
	void estimateSmoothFittingParameter(const SegmentConfiguration& segmentConfiguration,
		const int segmentIndex,
		const std::vector<int>& boundaryVariables,
		const rev::Image<unsigned char>& outlierImage,
		const std::vector< std::vector<double> >& disparityPlaneParameters,
		std::vector<double>& smoothfittedParameters) const;
	void interpolatePlaneParameters(const SegmentConfiguration& segmentConfiguration, std::vector< std::vector<double> >& disparityPlaneParameters) const;
	void makeBoundaryLabelImage(const SegmentConfiguration& segmentConfiguration, const std::vector<int>& boundaryLabels);
	void makeBoundaryLabelList(const SegmentConfiguration& segmentConfiguration, const std::vector<int>& boundaryLabels);

	// Parameter
	int iterationTotal_;
	double gridSize_;
	double compactnessWeight_;
	double disparityWeight_;
	double noDisparityPenalty_;

	// Data
	int labelTotal_;
	std::vector<int> labels_;

	// Color and disparity images
	rev::Image<float> firstLeftLabImage_;
	rev::Image<float> firstRightLabImage_;
	rev::Image<float> secondLeftLabImage_;
	rev::Image<float> firstLeftDisparityImage_;
	rev::Image<float> firstRightDisparityImage_;
	rev::Image<float> secondLeftDisparityImage_;
	std::vector<double> firstLeftLabEdges_;
	rev::Image<unsigned char> boundaryLabelImage_;
	std::vector< std::vector<int> > boundaryLabelList_;

	// Superpixel segments
	std::vector<LabXYD> seeds_;
	int stepSize_;

	rev::Image<unsigned char> outlierImage_;

	bool initialFlag_;
	void estimateOutlierImage(const SegmentConfiguration& segmentConfiguration,
		std::vector< std::vector<double> >& disparityPlaneParameters,
		rev::Image<unsigned char>& outlierImage);
};
